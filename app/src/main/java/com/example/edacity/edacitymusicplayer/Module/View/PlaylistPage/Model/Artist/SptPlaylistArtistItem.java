package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Artist;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistArtist;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 11/01/18.
 */

public class SptPlaylistArtistItem implements Playlistable{
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("artists")
    @Expose
    public List<SptPlaylistArtistEntity> artists = null;
    @SerializedName("duration_ms")
    @Expose
    public Integer durationMs;
    @SerializedName("track_number")
    @Expose
    public Integer trackNumber;
    @SerializedName("popularity")
    @Expose
    public Integer popularity;
    @SerializedName("uri")
    @Expose
    public String uri;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription1() {
        if (artists!=null){
            if (artists.size()>0){
                String s = "";
                for (SptPlaylistArtistEntity item: artists) {
                    s += item.name;
                    s += " ";
                }
                return s;
            }else {
                return "-";
            }
        }else{
            return "-";
        }
    }

    @Override
    public String getDescription2() {
        Integer durationS = durationMs/1000;
        String s = "duration: " + durationS.toString() + "s";
        return s;
    }

    @Override
    public ApiType getApiType() {
        return ApiType.ARTIST;
    }
}
