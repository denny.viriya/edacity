package com.example.edacity.edacitymusicplayer.Module.View.Playpage;

import com.example.edacity.edacitymusicplayer.Module.Service.Model.TrackInfo;

/**
 * Created by denny on 10/01/18.
 */

public interface TrackSubstanceView {
    public void onDataArrived(TrackInfo trackInfo);
}
