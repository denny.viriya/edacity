package com.example.edacity.edacitymusicplayer.Module.Network;

import com.example.edacity.edacitymusicplayer.Module.Model.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by denny on 10/12/17.
 */

public interface InteractorSpotify {
    @GET("/v1/me")
    Call<User> userDetail();
}
