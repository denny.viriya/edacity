package com.example.edacity.edacitymusicplayer.Module.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 10/12/17.
 */

public class User {
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("display_name")
    @Expose
    public Object displayName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("external_urls")
    @Expose
    public UserExternalUrls externalUrls;
    @SerializedName("followers")
    @Expose
    public UserFollowers followers;
    @SerializedName("href")
    @Expose
    public String href;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("images")
    @Expose
    public List<UserImage> images = null;
    @SerializedName("product")
    @Expose
    public String product;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("uri")
    @Expose
    public String uri;


}
