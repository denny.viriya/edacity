package com.example.edacity.edacitymusicplayer.Module.View.Activity;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorSpotify;
import com.example.edacity.edacitymusicplayer.Module.Model.User;
import com.example.edacity.edacitymusicplayer.Module.Utility.EdacityWebTokenHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denny on 10/12/17.
 */

public class MainPresenter{
    private EdacityWebTokenHelper edacityWebTokenHelper;
    private InteractorEdacity api;
    private InteractorSpotify spotifyApi;
    private MainView view;
    private User user;
    private SharedPreferences sharedPreferences;
    private Call<User> userCall;

    public MainPresenter(InteractorEdacity api, InteractorSpotify spotifyApi) {
        this.api = api;
        this.spotifyApi = spotifyApi;
        edacityWebTokenHelper = new EdacityWebTokenHelper();
    }

    public void bind(MainView view){
        this.view = view;
    }

    public void unBind(){
        this.view = null;
    }

    public void loadUserData(){
        userCall = spotifyApi.userDetail();
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                user = (User)response.body();
                view.userDataArrived(user);
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                Log.d("USER DATA", "onFailure() called with: call = [" + call + "], t = [" + t + "]");
            }
        });

    }

    public String createEdacityToken(){
        edacityWebTokenHelper.createToken(MainActivity.sharedPreferences
                .getString(MainActivity.SESSION_EMAIL,""));
        api.buildAuthToken(edacityWebTokenHelper.getClientToken());
        return edacityWebTokenHelper.getClientToken();
    }


}
