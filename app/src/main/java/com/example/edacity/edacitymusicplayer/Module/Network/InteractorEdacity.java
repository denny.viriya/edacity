package com.example.edacity.edacitymusicplayer.Module.Network;

import com.example.edacity.edacitymusicplayer.Module.Network.Request.PlaylistAddRequest;
import com.example.edacity.edacitymusicplayer.Module.Network.Request.PlaylistDeleteRequest;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.ApiResponse;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.Model.Tracks;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptCategoryList;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptFeaturedPlaylist;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistList;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptSearchItemList;
import com.example.edacity.edacitymusicplayer.Module.Service.Model.TrackInfo;
import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model.UserPlaylistData;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistAlbumTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistArtistTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchAlbumResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchArtistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchPlaylistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchTrackResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by denny on 09/12/17.
 */

public interface InteractorEdacity {

    void buildAuthToken(String token);

    @GET("/api/categories")
    Call<SptCategoryList> getCategories();

    @GET("/api/categoryplaylists/{categoryId}")
    Call<SptPlaylistList> getCategoryPlaylist(
            @Path("categoryId") String categoryId
    );

    @Deprecated
    @GET("/api/featuredplaylists?")
    Call<SptFeaturedPlaylist> featuredPlaylist(
            @Query("limit") int limit,
            @Query("page") int page);

    @GET("/api/search?")
    Call<SptSearchAlbumResponse> getSearchAlbum(
            @Query("type") String type,
            @Query("q") String q,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("/api/search?")
    Call<SptSearchArtistResponse> getSearchArtist(
            @Query("type") String type,
            @Query("q") String q,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("/api/search?")
    Call<SptSearchPlaylistResponse> getSearchPlaylist(
            @Query("type") String type,
            @Query("q") String q,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("/api/search?")
    Call<SptSearchTrackResponse> getSearchTrack(
            @Query("type") String type,
            @Query("q") String q,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("/api/album/{id}")
    Call<SptPlaylistAlbumTrackResponse> getAlbumTracks(
            @Path("id") String id
    );

    @GET("api/artist/{id}/top-tracks")
    Call<SptPlaylistArtistTrackResponse> getArtistTracks(
            @Path("id") String id
    );

    @POST("/api/create-playlist")
    Call<ApiResponse> addNewPlaylistToServer(
            @Body PlaylistAddRequest playlistAddRequest);

    @GET("/api/my-playlists")
    Call<UserPlaylistData> getMyPlaylist();

    @HTTP(method = "DELETE", path = "/api/delete-playlist", hasBody = true)
    Call<ApiResponse> deletePlaylistToServer(
            @Body PlaylistDeleteRequest playlistDeleteRequest);

    @GET("/api/user/{ownerid}/playlist/{playlistid}/tracks")
    Call<List<SptPlaylistTrack>> playlistTracks(
            @Path("ownerid") String ownerId,
            @Path("playlistid") String playlistId);

    @GET("api/track/{trackId}")
    Call<TrackInfo> getTrackInfo(
            @Path("trackId") String id);


}

//    String TRACK = "track";
//    String PLAYLIST = "playlist";
//    String ARTIST = "artist";
//    String ALBUM = "album";

//    @Deprecated
//    @GET("/api/toptracks?")
//    Call<Tracks> topTracks(
//            @Query("limit") int limit,
//            @Query("page") int page);
//
//    @Deprecated
//    @GET("/api/search/track/{keyword}?")
//    Call<Tracks> searchTracks(
//            @Path("keyword") String keyword,
//            @Query("limit") int limit,
//            @Query("page") int page);
//
//    @Deprecated
//    @GET("/api/search/album/{keyword}?")
//    Call<Tracks> searchAlbums(
//            @Path("keyword") String keyword,
//            @Query("limit") int limit,
//            @Query("page") int page);
//

//
//    @Deprecated
//    @GET("/api/search?")
//    Call<SptSearchItemList> getSearch(
//            @Query("type") String type,
//            @Query("q") String q,
//            @Query("limit") int limit,
//            @Query("offset") int offset
//    );