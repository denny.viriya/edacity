package com.example.edacity.edacitymusicplayer.Module.View.Homepage;

import android.os.Bundle;
import android.util.Log;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptCategory;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptCategoryList;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistList;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Homepage.Adapter.RecyclerviewPresenter;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.PlaylistFragment;

import org.parceler.Parcels;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denny on 09/12/17.
 */

public class HomePresenter implements RecyclerviewPresenter {
    private static final String TAG = "HOME";
    private InteractorEdacity api;
    private SptCategoryList sptCategoryList;
    private SptPlaylistList sptPlaylistList1;
    private SptPlaylistList sptPlaylistList2;
    private SptPlaylistList sptPlaylistList3;
    private SptPlaylistList sptPlaylistList4;
    private Call<SptCategoryList> sptCategoryCall;
    private Call<SptPlaylistList> sptPlaylistCall1;
    private Call<SptPlaylistList> sptPlaylistCall2;
    private Call<SptPlaylistList> sptPlaylistCall3;
    private Call<SptPlaylistList> sptPlaylistCall4;
    private HomeView view;
    private Bundle playlistDataBundle;
    private SptItemBasic compatPlaylist;

    public HomePresenter(InteractorEdacity api){
        this.api = api;
    }

    void bind(HomeView view){
        this.view = view;
    }

    void unBind(){
        this.view = null;
    }

    void loadData(){
        Log.d(TAG, "loadData() called");
        sptCategoryCall = api.getCategories();
        sptCategoryCall.enqueue(new Callback<SptCategoryList>() {
            @Override
            public void onResponse(Call<SptCategoryList> call, Response<SptCategoryList> response) {
                sptCategoryList = response.body();
                loadCategoryContent(sptCategoryList.categoryList);
                view.updateTitle(sptCategoryList.categoryList);
                Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<SptCategoryList> call, Throwable t) {

            }
        });
    }

    void cancelAllCall(){
        if (sptCategoryCall!=null){
            if (sptCategoryCall.isExecuted()){
                sptCategoryCall.cancel();
            }
        }
        if (sptPlaylistCall1!=null){
            if (sptPlaylistCall1.isExecuted()){
                sptPlaylistCall1.cancel();
            }
        }
        if (sptPlaylistCall2!=null){
            if (sptPlaylistCall2.isExecuted()){
                sptPlaylistCall2.cancel();
            }
        }
        if (sptPlaylistCall3!=null){
            if (sptPlaylistCall3.isExecuted()){
                sptPlaylistCall3.cancel();
            }
        }
        if (sptPlaylistCall4!=null){
            if (sptPlaylistCall4.isExecuted()){
                sptPlaylistCall4.cancel();
            }
        }
    }

    private void loadCategoryContent(List<SptCategory> categories){
        Log.d(TAG, "loadCategoryContent: ");
        sptPlaylistCall1 = api.getCategoryPlaylist(categories.get(0).id);
        sptPlaylistCall2 = api.getCategoryPlaylist(categories.get(1).id);
        sptPlaylistCall3 = api.getCategoryPlaylist(categories.get(2).id);
        sptPlaylistCall4 = api.getCategoryPlaylist(categories.get(3).id);

        sptPlaylistCall1.enqueue(new Callback<SptPlaylistList>() {
            @Override
            public void onResponse(Call<SptPlaylistList> call, Response<SptPlaylistList> response) {
                sptPlaylistList1 = response.body();
                view.updateCardview(sptPlaylistList1.playlistList, 1);
                Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            }

            @Override
            public void onFailure(Call<SptPlaylistList> call, Throwable t) {

            }
        });

        sptPlaylistCall2.enqueue(new Callback<SptPlaylistList>() {
            @Override
            public void onResponse(Call<SptPlaylistList> call, Response<SptPlaylistList> response) {
                sptPlaylistList2 = response.body();
                view.updateCardview(sptPlaylistList2.playlistList, 2);
            }

            @Override
            public void onFailure(Call<SptPlaylistList> call, Throwable t) {

            }
        });

        sptPlaylistCall3.enqueue(new Callback<SptPlaylistList>() {
            @Override
            public void onResponse(Call<SptPlaylistList> call, Response<SptPlaylistList> response) {
                sptPlaylistList3 = response.body();
                view.updateCardview(sptPlaylistList3.playlistList, 3);
            }

            @Override
            public void onFailure(Call<SptPlaylistList> call, Throwable t) {

            }
        });

        sptPlaylistCall4.enqueue(new Callback<SptPlaylistList>() {
            @Override
            public void onResponse(Call<SptPlaylistList> call, Response<SptPlaylistList> response) {
                sptPlaylistList4 = response.body();
                view.updateCardview(sptPlaylistList4.playlistList, 4);
            }

            @Override
            public void onFailure(Call<SptPlaylistList> call, Throwable t) {

            }
        });
    }

    /**
     * dipanggil dari
     * @see com.example.edacity.edacitymusicplayer.Module.View.Homepage.Adapter.RecyclerViewAdapter
     */
    @Override
    public void openSelectedPlaylist(SptPlaylist sptPlaylist) {
        playlistDataBundle = new Bundle();
        compatPlaylist = new SptItemBasic();

        compatPlaylist
                .setId(sptPlaylist.id)
                .setName(sptPlaylist.name)
                .setOwnerId(sptPlaylist.ownerId)
                .setApiType(ApiType.PLAYLIST);
        if (sptPlaylist.images.size()>0){
            compatPlaylist.setImageUrl(sptPlaylist.images.get(0).url);
        }
        playlistDataBundle.putParcelable(MainActivity.PLAYLIST_KEYWORD, Parcels.wrap(compatPlaylist));
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(playlistDataBundle);

        view.openPlaylistFragment(fragment);
    }
}
