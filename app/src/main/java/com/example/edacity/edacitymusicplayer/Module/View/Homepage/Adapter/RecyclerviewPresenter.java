package com.example.edacity.edacitymusicplayer.Module.View.Homepage.Adapter;

import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;

/**
 * Created by denny on 10/12/17.
 */

public interface RecyclerviewPresenter {
    void openSelectedPlaylist(SptPlaylist sptPlaylist);
}
