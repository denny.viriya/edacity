package com.example.edacity.edacitymusicplayer.Module.View.Searchpage;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;

import com.example.edacity.edacitymusicplayer.Module.Model.Tracks;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptSearchItem;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptSearchItemList;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchAlbumResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchArtistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchPlaylistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchAlbum;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchArtist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchTrack;

import java.util.List;

/**
 * Created by denny on 10/12/17.
 */

public interface SearchView {
    void hideProgressBar();
    void showProgressbar();
    void cleanSearchResult();
    void appendResultTrack(SptSearchTrackResponse response);
    void appendResultArtist(SptSearchArtistResponse response);
    void appendResultAlbum(SptSearchAlbumResponse response);
    void appendResultPlaylist(SptSearchPlaylistResponse response);
    void playSingleTrack(List<Playlistable> playlistable);
    void openPlaylistFragment(Fragment fragment);
}
//    void openPlaylistFragment(SptSearchPlaylist playlist, Bundle bundle);
//    void openPlaylistFragment(SptSearchAlbum album, Bundle bundle);
//    void openPlaylistFragment(SptSearchTrack track, Bundle bundle);
//    void openPlaylistFragment(SptSearchArtist artist, Bundle bundle);