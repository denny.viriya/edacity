package com.example.edacity.edacitymusicplayer.Module.View.Homepage;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptCategory;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptFeaturedPlaylist;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivityViews;
import com.example.edacity.edacitymusicplayer.Module.View.Homepage.Adapter.RecyclerViewAdapter;

import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.PlaylistFragment;
import com.example.edacity.edacitymusicplayer.R;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by denny on 02/11/17.
 * @author Denny
 */

public class HomeFragment extends Fragment implements HomeView{

    private RecyclerView mRecyclerView1, mRecyclerView2, mRecyclerView3, mRecyclerView4;
    private TextView titleRv1, titleRv2, titleRv3, titleRv4;
    private View progressBar;
    private View homeScreen;
    private InteractorEdacity api;
    public static HomePresenter presenter;
    private FragmentManager fragmentManager;
    private Bundle playlistDataForFragment;
    private PlaylistFragment playlistFragment;
    private MainActivityViews mainActivityViews;
    public static final String PLAYLIST_KEY = "playlist_data";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivityViews = (MainActivityViews) context;
    }

    /**
     * ini untuk SDK<23
     * SDK23++ menggunakan Context, atau gunakan support library untuk bug ini
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (mainActivityViews==null){
            mainActivityViews = (MainActivityViews) activity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        initView(view);
        mainActivityViews.toolbarHasBackButton(false);
        mainActivityViews.setTitle("Home");
        api = MainActivity.api;
        presenter = new HomePresenter(api);
        presenter.bind(this);

        showView(progressBar);
        hideView(homeScreen);

        initiateBlankPlaylistCardview(getActivity());
        presenter.loadData();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideView(progressBar);
                showView(homeScreen);
            }
        }, 2000);


        return view;
    }

    void initView(View view){
        progressBar = view.findViewById(R.id.home_progress_bar);
        homeScreen = view.findViewById(R.id.home_screen);
        titleRv1 = view.findViewById(R.id.home_title_playlist);
        titleRv2 = view.findViewById(R.id.home_title_playlist2);
        titleRv3 = view.findViewById(R.id.home_title_playlist3);
        titleRv4 = view.findViewById(R.id.home_title_playlist4);
        mRecyclerView1 = view.findViewById(R.id.home_recyclerview_playlist);
        mRecyclerView2 = view.findViewById(R.id.home_recyclerview_playlist2);
        mRecyclerView3= view.findViewById(R.id.home_recyclerview_playlist3);
        mRecyclerView4 = view.findViewById(R.id.home_recyclerview_playlist4);
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.cancelAllCall();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unBind();
    }

    private void createPlaylistCardview(RecyclerView recyclerView, @Nullable List<SptPlaylist> sptPlaylistList, Context context) {
        recyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.HORIZONTAL,
                false
        ));
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(sptPlaylistList, context);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void initiateBlankPlaylistCardview(Context context){
        createPlaylistCardview(mRecyclerView1, null, context);
        createPlaylistCardview(mRecyclerView2, null, context);
        createPlaylistCardview(mRecyclerView3, null, context);
        createPlaylistCardview(mRecyclerView4, null, context);
    }

    @Override
    public void updateTitle(@NonNull List<SptCategory> sptCategoryList) {
        titleRv1.setText(sptCategoryList.get(0).name);
        titleRv2.setText(sptCategoryList.get(1).name);
        titleRv3.setText(sptCategoryList.get(2).name);
        titleRv4.setText(sptCategoryList.get(3).name);
        hideView(progressBar);
        showView(homeScreen);
    }

    @Override
    public void updateCardview(List<SptPlaylist> sptPlaylistList, int recyclerView) {
        switch (recyclerView){
            case 1:{
                createPlaylistCardview(mRecyclerView1, sptPlaylistList, getActivity());
                break;
            }
            case 2:{
                createPlaylistCardview(mRecyclerView2, sptPlaylistList, getActivity());
                break;
            }
            case 3:{
                createPlaylistCardview(mRecyclerView3, sptPlaylistList, getActivity());
                break;
            }
            case 4:{
                createPlaylistCardview(mRecyclerView4, sptPlaylistList, getActivity());
                break;
            }

        }
    }

    @Override
    public void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    @Override
    public void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void openPlaylistFragment(Fragment fragment) {
//        playlistDataForFragment = new Bundle();
//        playlistDataForFragment.putParcelable(PLAYLIST_KEY, Parcels.wrap(sptPlaylist));
//
//        playlistFragment = new PlaylistFragment();
//        playlistFragment.setArguments(playlistDataForFragment);
//
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment,fragment)
                .addToBackStack("home1")
                .commit();
    }

}


