package com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model;

import com.example.edacity.edacitymusicplayer.Module.Model.SptItem;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptImage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by denny on 01/01/18.
 */
@Parcel
public class SptSearchArtist extends SptItem {
    @SerializedName("external_urls")
    @Expose
    public SptSearchExternalUrl externalUrls;

    @SerializedName("href")
    @Expose
    public String href;

    @SerializedName("uri")
    @Expose
    public String uri;

    @SerializedName("popularity")
    @Expose
    public Integer popularity;

    @SerializedName("images")
    @Expose
    public List<SptImage> images = null;

    @SerializedName("genres")
    @Expose
    public List<String> genres = null;
}
