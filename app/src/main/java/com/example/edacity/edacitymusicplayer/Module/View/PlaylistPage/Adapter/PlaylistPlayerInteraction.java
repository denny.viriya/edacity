package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Adapter;

import android.support.annotation.NonNull;

import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;

import java.util.List;

/**
 * Created by denny on 24/12/17.
 */

public interface PlaylistPlayerInteraction {
    public void onTrackSelected(@NonNull List<Playlistable> playlistableList, int position);
}
