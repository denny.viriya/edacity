package com.example.edacity.edacitymusicplayer.Module.Network.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 30/12/17.
 */

public class SptPlaylistList {
    @SerializedName("items")
    @Expose
    public List<SptPlaylist> playlistList;
}
