package com.example.edacity.edacitymusicplayer.Module.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denny on 10/12/17.
 */

public class UserExternalUrls {
    @SerializedName("spotify")
    @Expose
    public String spotify;
}
