package com.example.edacity.edacitymusicplayer.Module.Network;

import com.example.edacity.edacitymusicplayer.Module.Model.User;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by denny on 10/12/17.
 */

public class SpotifyAPI implements InteractorSpotify {
    private static final String SPOTIFY_AUTH = "https://api.spotify.com/";
    private InteractorSpotify spotifyAPI;
    private String accessToken;
    private OkHttpClient.Builder builder;
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private HttpLoggingInterceptor loggingInterceptor;


    public SpotifyAPI(String accessToken) {
        this.accessToken = accessToken;
        buildAuthHttpClient(accessToken);
    }

    public void buildAuthHttpClient(final String accessToken){
        this.accessToken = accessToken;
//        loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder = new OkHttpClient.Builder();
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request userRequest = originalRequest.newBuilder()
                        .header("Authorization", "Bearer " + accessToken)
                        .method(originalRequest.method(), originalRequest.body())
                        .build();

                Response response = chain.proceed(userRequest);
                return response;
            }
        });

//        builder.addInterceptor(loggingInterceptor);

        okHttpClient = builder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(SPOTIFY_AUTH)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        spotifyAPI = retrofit.create(InteractorSpotify.class);

    }

    @Override
    public Call<User> userDetail() {
        return spotifyAPI.userDetail();
    }
}
