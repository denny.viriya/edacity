package com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.edacity.edacitymusicplayer.R;

/**
 * Created by denny on 10/12/17.
 */

public class AddPlaylistDialog extends DialogFragment {
    private LayoutInflater inflater;
    private AlertDialog.Builder builder;
    private Button button;
    private ImageButton buttonClose;
    private EditText editText;
    private MyLibraryPresenter presenter;
    private String playlistName;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(getActivity());
        inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_playlist, null);
        initView(view);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playlistName = String.valueOf(editText.getText());
                if (!(playlistName.isEmpty())){
                    //jangan simpan ke SharedPref lagi
//                    presenter.addNewPlaylist(playlistName);
                    presenter.addPlaylistToServer(playlistName);
                    getDialog().dismiss();
                }
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });

        builder.setView(view);
        return builder.create();
    }

    private void initView(View view){
        button = view.findViewById(R.id.button_playlist_add);
        editText = view.findViewById(R.id.playlist_name);
        buttonClose = view.findViewById(R.id.button_playlist_close);
        presenter = MyLibraryFragment.presenter;
    }
}
