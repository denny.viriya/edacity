package com.example.edacity.edacitymusicplayer.Module.View.Homepage.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.Homepage.HomeFragment;
import com.example.edacity.edacitymusicplayer.R;

import java.util.List;

/**
 * Created by denny on 02/11/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private Context context;
    private RecyclerviewPresenter presenter;
    private List<SptPlaylist> sptPlaylistList;

    public RecyclerViewAdapter(@Nullable List<SptPlaylist> sptPlaylistsList,
                               @NonNull Context context){
        this.sptPlaylistList = sptPlaylistsList;
        this.context = context;
        presenter = HomeFragment.presenter;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.playlist_cardview, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
        if (sptPlaylistList!=null){
            String imageUrl = sptPlaylistList.get(position).images.get(0).url;
            Glide.with(context).load(imageUrl).into(holder.cardViewImage);
            holder.cardViewTitle.setText(sptPlaylistList.get(position).name);
            holder.cardViewExplanation.setText(sptPlaylistList.get(position).id);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.openSelectedPlaylist(sptPlaylistList.get(holder.getAdapterPosition()));
                }
            });

            holder.cardViewImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.openSelectedPlaylist(sptPlaylistList.get(holder.getAdapterPosition()));
                }
            });
        }
        else {
            holder.cardViewImage.setImageResource(R.drawable.playlist1);
            holder.cardViewTitle.setText("Loading..");
            holder.cardViewExplanation.setText("is Loading also...");
        }

    }


    @Override
    public int getItemCount() {
        if (sptPlaylistList!=null){
            return sptPlaylistList.size();
        }else {
            return 5;
        }
    }
}

/**
 * merupakan viewHolder dari CardView yang telah dicustom
 *
 */
class CustomViewHolder extends RecyclerView.ViewHolder{

    ImageButton cardViewImage;
    TextView cardViewTitle;
    TextView cardViewExplanation;

    CustomViewHolder(View itemView) {
        super(itemView);
        cardViewImage = itemView.findViewById(R.id.cardview_image);
        cardViewTitle = itemView.findViewById(R.id.cardview_title);
        cardViewExplanation = itemView.findViewById(R.id.cardview_explanation);
    }

}
