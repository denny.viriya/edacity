package com.example.edacity.edacitymusicplayer.Module.Model;

import org.parceler.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 26/11/17.
 */
@Parcel
public class Tracks {

    @SerializedName("results")
    @Expose
    List<Result> results = null;

    public List<Result> getResults() {
        return results;
    }

}




