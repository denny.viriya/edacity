package com.example.edacity.edacitymusicplayer.Module.Model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.parceler.Parcel;

/**
 * Created by denny on 01/01/18.
 */
@Parcel
public class SptItemBasic extends SptItem {
    public String imageUrl;
    public String ownerId;
    public ApiType apitype;

    public SptItemBasic() {
    }

    public SptItemBasic setId(@NonNull String id){
        this.id = id;
        return this;
    }

    public SptItemBasic setName(@NonNull String name){
        this.name = name;
        return this;
    }

    public SptItemBasic setImageUrl(@Nullable String imageUrl) {
        if (!isBlank(imageUrl)){
            this.imageUrl = imageUrl;
        }
        return this;
    }

    public SptItemBasic setOwnerId(@Nullable String ownerId) {
        if (!isBlank(ownerId)){
            this.ownerId = ownerId;
        }
        return this;
    }

    public SptItemBasic setType(@Nullable String type){
        if (!isBlank(type)){
            this.type = type;
        }
        return this;
    }

    public SptItemBasic setApiType(@NonNull ApiType apiType){
        this.apitype = apiType;
        return this;
    }

    private boolean isBlank(@Nullable String s){
        if (s==null)
            return true;
        else if (s.isEmpty())
            return true;
        else
            return false;
    }
}
