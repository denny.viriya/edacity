package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistAlbumTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistArtistTrackResponse;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denny on 23/12/17.
 */

public class PlaylistPagePresenter{
    private static final String TAG = "PLAYLIST PRESENTER";
    private InteractorEdacity api;
    private PlaylistPageView view;

    public PlaylistPagePresenter(InteractorEdacity api) {
        this.api = api;
    }

    public void bind(PlaylistPageView view){
        this.view = view;
    }

    public void unBind(){
        this.view = null;
    }

    public void loadPlaylistTracks(SptItemBasic sptItemBasic){
        switch (sptItemBasic.apitype){
            case ALBUM:{
                Call<SptPlaylistAlbumTrackResponse> albumTrackResponseCall = api.getAlbumTracks(sptItemBasic.id);
                albumTrackResponseCall.enqueue(new Callback<SptPlaylistAlbumTrackResponse>() {
                    @Override
                    public void onResponse(Call<SptPlaylistAlbumTrackResponse> call, Response<SptPlaylistAlbumTrackResponse> response) {
                        SptPlaylistAlbumTrackResponse albumTrackResponse = response.body();
                        view.updateUi(albumTrackResponse);
                    }

                    @Override
                    public void onFailure(Call<SptPlaylistAlbumTrackResponse> call, Throwable t) {

                    }
                });
                break;
            }
            case TRACK:{
                //do nothing, tidak akan dipanggil
                break;
            }
            case PLAYLIST:{
                Call<List<SptPlaylistTrack>> playlistTracksCall = api.playlistTracks(sptItemBasic.ownerId, sptItemBasic.id);
                playlistTracksCall.enqueue(new Callback<List<SptPlaylistTrack>>() {
                    @Override
                    public void onResponse(Call<List<SptPlaylistTrack>> call, Response<List<SptPlaylistTrack>> response) {
                        List<SptPlaylistTrack> sptPlaylistTrackList = response.body();
                        view.updateUi(sptPlaylistTrackList);
                    }

                    @Override
                    public void onFailure(Call<List<SptPlaylistTrack>> call, Throwable t) {

                    }
                });
                break;
            }
            case ARTIST:{
                Call<SptPlaylistArtistTrackResponse> artistTrackResponseCall = api.getArtistTracks(sptItemBasic.id);
                artistTrackResponseCall.enqueue(new Callback<SptPlaylistArtistTrackResponse>() {
                    @Override
                    public void onResponse(Call<SptPlaylistArtistTrackResponse> call, Response<SptPlaylistArtistTrackResponse> response) {
                        SptPlaylistArtistTrackResponse artistTrackResponse = response.body();
                        view.updateUi(artistTrackResponse);
                    }

                    @Override
                    public void onFailure(Call<SptPlaylistArtistTrackResponse> call, Throwable t) {

                    }
                });
                break;
            }
        }
    }



    public void onPlaylistItemSelected(List<Playlistable> playlistableList, int position){
        if (playlistableList!=null){
            view.onItemSelected(playlistableList, position);
        }
    }

}
