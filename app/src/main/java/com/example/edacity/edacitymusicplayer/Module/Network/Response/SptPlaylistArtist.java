package com.example.edacity.edacitymusicplayer.Module.Network.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denny on 23/12/17.
 */

public class SptPlaylistArtist {
    @SerializedName("artist_id")
    @Expose
    public String artistId;
    @SerializedName("artist_name")
    @Expose
    public String artistName;
}
