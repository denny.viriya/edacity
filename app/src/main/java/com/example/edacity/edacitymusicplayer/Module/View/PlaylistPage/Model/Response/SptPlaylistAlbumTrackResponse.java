package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response;

import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Album.SptPlaylistAlbumItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 02/01/18.
 */

public class SptPlaylistAlbumTrackResponse {

    @SerializedName("href")
    @Expose
    public String href;

    @SerializedName("items")
    @Expose
    public List<SptPlaylistAlbumItem> items = null;

    @SerializedName("limit")
    @Expose
    public Integer limit;

    @SerializedName("next")
    @Expose
    public Object next;

    @SerializedName("offset")
    @Expose
    public Integer offset;

    @SerializedName("previous")
    @Expose
    public Object previous;

    @SerializedName("total")
    @Expose
    public Integer total;
}
