package com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorSpotify;
import com.example.edacity.edacitymusicplayer.Module.Model.User;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivityViews;
import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model.UserPlaylistData;
import com.example.edacity.edacitymusicplayer.R;

import java.util.ArrayList;

/**
 * Created by denny on 29/11/17.
 */

public class MyLibraryFragment extends Fragment implements MyLibraryView {
    private ImageView imageView;
    private TextView name, email;
    private Button addPlaylistButton;
    static MyLibraryPresenter presenter;
    private InteractorEdacity api;
    private InteractorSpotify spotifyAPI;
    private ImageButton dialogPlaylistButton;
    private AddPlaylistDialog addPlaylistDialog;
    private final String DIALOG_TAG = "open_dialog";
    private RecyclerView playlistRecyclerView;
    private MylibraryRecyclerViewAdapter playlistAdapter;
    private MainActivityViews mainActivityViews;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivityViews = (MainActivityViews) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivityViews = (MainActivityViews) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_library_fragment, container, false);
        initView(view);
        mainActivityViews.toolbarHasBackButton(false);
        mainActivityViews.setTitle("My Library");

        api = MainActivity.api;
        spotifyAPI = MainActivity.spotifyApi;
        presenter = new MyLibraryPresenter(api, spotifyAPI);
        presenter.bind(this);
        presenter.loadUserData();

        dialogPlaylistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPlaylistDialog = new AddPlaylistDialog();
                addPlaylistDialog.show(getFragmentManager(), DIALOG_TAG);
            }
        });

        return view;
    }

    private void initView(View view){
        imageView = view.findViewById(R.id.image_profile);
        name = view.findViewById(R.id.name_profile);
        email = view.findViewById(R.id.email_profile);
        dialogPlaylistButton = view.findViewById(R.id.button_playlist_dialog);
        addPlaylistButton = view.findViewById(R.id.button_playlist_add);
        playlistRecyclerView = view.findViewById(R.id.my_library_playlist);

        playlistRecyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false
        ));
    }

    @Override
    public void updateUserUi(User user) {
        Uri imageUrl = Uri.parse(user.images.get(0).url);
        Glide.with(getActivity())
                .load(imageUrl)
                .apply(RequestOptions.circleCropTransform())
                .into(imageView);

        name.setText(MainActivity.sharedPreferences.getString(MainActivity.SESSION_NAME, "null"));
        email.setText(MainActivity.sharedPreferences.getString(MainActivity.SESSION_EMAIL, "null"));
    }

    @Override
    public void updatePlaylistUi(UserPlaylistData userPlaylist) {
        if (playlistAdapter==null){
            playlistAdapter = new MylibraryRecyclerViewAdapter(userPlaylist);
        }else {
            playlistAdapter.updateDataset(userPlaylist);
        }
        Toast.makeText(getActivity(),"UPDATE UI", Toast.LENGTH_SHORT).show();
        playlistRecyclerView.setAdapter(playlistAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unBind();
    }
}
