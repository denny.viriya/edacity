package com.example.edacity.edacitymusicplayer.Module.Network;


import android.support.annotation.NonNull;

import com.example.edacity.edacitymusicplayer.Module.Network.Request.PlaylistAddRequest;
import com.example.edacity.edacitymusicplayer.Module.Network.Request.PlaylistDeleteRequest;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.ApiResponse;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.Model.Tracks;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptCategoryList;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptFeaturedPlaylist;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistList;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptSearchItemList;
import com.example.edacity.edacitymusicplayer.Module.Service.Model.TrackInfo;
import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model.UserPlaylistData;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistAlbumTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistArtistTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchAlbumResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchArtistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchPlaylistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchTrackResponse;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by denny on 07/12/17.
 */

public class API implements InteractorEdacity{
    private static final String BASE_URL = "http://192.168.100.104:8008/";
//    private static final String BASE_URL = "http://192.168.43.201:8008/";
//    private static final String BASE_URL = "http://172.22.185.73:8008/";
    private static final int TOP_TRACK_COUNT = 10;
    private PlaylistAddRequest playlistAddRequest;
    private Retrofit retrofit, authenticatedRetrofit;
    private InteractorEdacity backendApi, authenticatedBackEndApi;

    public API() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        backendApi = retrofit.create(InteractorEdacity.class);

    }

    public void createAuthenticatedApi(final String token){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new Interceptor() {
        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request authenticatedRequest = originalRequest.newBuilder()
                    .header("authorization", token)
                    .method(originalRequest.method(), originalRequest.body())
                    .build();

            Response response = chain.proceed(authenticatedRequest);
            return response;
            }
        });
        builder.addInterceptor(loggingInterceptor);

        OkHttpClient okHttpClient = builder.build();
        authenticatedRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        authenticatedBackEndApi = authenticatedRetrofit.create(InteractorEdacity.class);
    }

    @Override
    public void buildAuthToken(String token) {
        createAuthenticatedApi(token);
    }

    @Override
    public Call<SptFeaturedPlaylist> featuredPlaylist(int limit, int page) {
        return backendApi.featuredPlaylist(TOP_TRACK_COUNT, 1);
    }

    @Override
    public Call<List<SptPlaylistTrack>> playlistTracks(String ownerId, String playlistId) {
        return backendApi.playlistTracks(ownerId, playlistId);
    }

    @Override
    public Call<SptCategoryList> getCategories() {
        return backendApi.getCategories();
    }

    @Override
    public Call<SptPlaylistList> getCategoryPlaylist(String categoryId) {
        return backendApi.getCategoryPlaylist(categoryId);
    }



    @Override
    public Call<SptSearchAlbumResponse> getSearchAlbum(String type, String q, int limit, int offset) {
        return backendApi.getSearchAlbum(type, q, limit, offset);
    }

    @Override
    public Call<SptSearchArtistResponse> getSearchArtist(String type, String q, int limit, int offset) {
        return backendApi.getSearchArtist(type, q, limit, offset);
    }

    @Override
    public Call<SptSearchPlaylistResponse> getSearchPlaylist(String type, String q, int limit, int offset) {
        return backendApi.getSearchPlaylist(type, q, limit, offset);
    }

    @Override
    public Call<SptSearchTrackResponse> getSearchTrack(String type, String q, int limit, int offset) {
        return backendApi.getSearchTrack(type, q, limit, offset);
    }

    @Override
    public Call<SptPlaylistAlbumTrackResponse> getAlbumTracks(String id) {
        return backendApi.getAlbumTracks(id);
    }

    @Override
    public Call<SptPlaylistArtistTrackResponse> getArtistTracks(String id) {
        return backendApi.getArtistTracks(id);
    }

    @Override
    public Call<ApiResponse> addNewPlaylistToServer(PlaylistAddRequest playlistAddRequest) {
        return authenticatedBackEndApi.addNewPlaylistToServer(playlistAddRequest);
    }

    @Override
    public Call<UserPlaylistData> getMyPlaylist() {
        return authenticatedBackEndApi.getMyPlaylist();
    }

    @Override
    public Call<ApiResponse> deletePlaylistToServer(PlaylistDeleteRequest playlistDeleteRequest) {
        return authenticatedBackEndApi.deletePlaylistToServer(playlistDeleteRequest);
    }

    @Override
    public Call<TrackInfo> getTrackInfo(String id) {
        return backendApi.getTrackInfo(id);
    }
}

//    @Deprecated
//    @Override
//    public Call<Tracks> topTracks(int limit, int page) {
//        return backendApi.topTracks(TOP_TRACK_COUNT, 1);
//    }
//
//    @Deprecated
//    @Override
//    public Call<Tracks> searchTracks(String keyword, int limit, int page) {
//        return backendApi.searchTracks(keyword, TOP_TRACK_COUNT, 1);
//    }
//
//    @Deprecated
//    @Override
//    public Call<Tracks> searchAlbums(String keyword, int limit, int page) {
//        return backendApi.searchAlbums(keyword, TOP_TRACK_COUNT, 1);
//    }

//    @Deprecated
//    @Override
//    public Call<SptSearchItemList> getSearch(String type, String q, int limit, int offset) {
//        return backendApi.getSearch(type, q, limit, offset);
//    }