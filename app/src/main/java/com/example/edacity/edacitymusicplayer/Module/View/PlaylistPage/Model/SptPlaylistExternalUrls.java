package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denny on 02/01/18.
 */

public class SptPlaylistExternalUrls {
    @SerializedName("spotify")
    @Expose
    public String spotify;
}
