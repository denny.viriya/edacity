package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;

import org.parceler.Parcel;

/**
 * Created by denny on 03/01/18.
 */

public interface Playlistable {
    String getId();
    String getName();
    String getDescription1();
    String getDescription2();
    ApiType getApiType();
}
