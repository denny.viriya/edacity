package com.example.edacity.edacitymusicplayer.Module.Network.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by denny on 11/12/17.
 */
@Parcel
public class SptImage {
    @SerializedName("height")
    @Expose
    public Integer height;

    @SerializedName("url")
    @Expose
    public String url;

    @SerializedName("width")
    @Expose
    public Integer width;
}
