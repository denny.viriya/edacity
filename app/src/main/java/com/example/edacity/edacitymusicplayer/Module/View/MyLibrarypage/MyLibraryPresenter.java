package com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorSpotify;
import com.example.edacity.edacitymusicplayer.Module.Model.User;
import com.example.edacity.edacitymusicplayer.Module.Network.Request.PlaylistAddRequest;
import com.example.edacity.edacitymusicplayer.Module.Network.Request.PlaylistDeleteRequest;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.ApiResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model.UserPlaylistData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denny on 10/12/17.
 */

public class MyLibraryPresenter {
    private static final String TAG = "My Lib Presenter";
    private InteractorSpotify spotifyAPI;
    private InteractorEdacity api;
    private MyLibraryView view;
    private User user;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final String PLAYLIST_KEY = "myLibrary_playlist";
    private ArrayList<String> userPlaylist;
    private String _userPlayList;
    private Gson gson;
    private Type userPlaylistType;
    private PlaylistAddRequest playlistAddRequest;
    private PlaylistDeleteRequest playlistDeleteRequest;

    private UserPlaylistData userPlaylistData;

    public MyLibraryPresenter(InteractorEdacity api, InteractorSpotify spotifyAPI) {
        this.spotifyAPI = spotifyAPI;
        this.api = api;

        sharedPreferences = MainActivity.sharedPreferences;
        gson = new Gson();
        userPlaylistType = new TypeToken<ArrayList<String>>(){}.getType();
        _userPlayList = sharedPreferences.getString(PLAYLIST_KEY, null);

        if (_userPlayList==null){
            userPlaylist = new ArrayList<>();
        }else {
            userPlaylist = gson.fromJson(_userPlayList, userPlaylistType);
        }
    }

    public void bind(MyLibraryView view){
        this.view = view;
    }

    public void unBind(){
        this.view = null;
    }

    public void loadUserData(){
        Call<User> userCall = spotifyAPI.userDetail();
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                user = (User)response.body();
                view.updateUserUi(user);
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                Log.d("USER DATA", "onFailure() called with: call = [" + call + "], t = [" + t + "]");
            }
        });

        getUserPlaylistData();

    }

    private void getUserPlaylistData(){
        Call<UserPlaylistData> mylibraryCall = api.getMyPlaylist();
        mylibraryCall.enqueue(new Callback<UserPlaylistData>() {
            @Override
            public void onResponse(Call<UserPlaylistData> call, Response<UserPlaylistData> response) {
                userPlaylistData = response.body();
                if (response.code()==200){
                    updateLocalPlaylist(userPlaylistData);
                }
            }

            @Override
            public void onFailure(Call<UserPlaylistData> call, Throwable t) {

            }
        });
    }

    public void updateLocalPlaylist(UserPlaylistData userPlaylistData){
        view.updatePlaylistUi(userPlaylistData);
    }

    public void deletePlaylist(UserPlaylistData userPlaylist, int position){
        playlistDeleteRequest = new PlaylistDeleteRequest(userPlaylist.playlists.get(position).id);
        Call<ApiResponse> apiResponseCall = api.deletePlaylistToServer(playlistDeleteRequest);
        Log.d(TAG, "deletePlaylist: CALLED");
        apiResponseCall.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.code()==200){
                    getUserPlaylistData();
                    Log.i(TAG, "onResponse: DELETE SUCCESS");
                }
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
//        view.updatePlaylistUi(userPlaylistData);
    }

    public void addPlaylistToServer(String playlist){
        playlistAddRequest = new PlaylistAddRequest(playlist);
        Call<ApiResponse> apiResponseCall = api.addNewPlaylistToServer(playlistAddRequest);

        apiResponseCall.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.code()==200){
                    getUserPlaylistData();
                    Log.i(TAG, "onResponse: ADD SUCCESS");
                }
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {

            }
        });
    }
}
