package com.example.edacity.edacitymusicplayer.Module.Utility;


import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


/**
 * Created by denny on 26/12/17.
 */

public class EdacityWebTokenHelper {
    private static final String TAG = "JWT";
    private final String secret = "396415bf72d1417aaaf35e356ebdeec7";
    private String sJsonWebToken;
    private Map<String, Object> map;

    public void createToken(String email){
        map = new HashMap<>();
        map.put("email",email);
        sJsonWebToken = Jwts.builder()
                .addClaims(map)
                .signWith(SignatureAlgorithm.HS256, secret.getBytes())
                .compact();
    }

    public String getClientToken() {
        return sJsonWebToken;
    }

    public boolean isClientTokenNull(){
        if (sJsonWebToken == null){
            return true;
        }
        return false;
    }
}


