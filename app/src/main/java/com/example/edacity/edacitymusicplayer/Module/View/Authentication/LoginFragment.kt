package com.example.edacity.edacitymusicplayer.Module.View.Authentication

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.edacity.edacitymusicplayer.R

/**
 * Created by denny on 02/11/17.
 * Fragment untuk melakukan login pertama kali
 * @author  Edwin
 */
class LoginFragment : Fragment() {
    private lateinit var loginCommunicator: AuthContracts.LoginFragmentCommunicator


    override fun onAttach(context: Context) {
        super.onAttach(context)
        loginCommunicator = context as AuthContracts.LoginFragmentCommunicator
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.login_form, container, false)
        val window = activity!!.window

        val spotifyButton = view.findViewById<Button>(R.id.custom_facebook_login_button)
        val appButton = view.findViewById<Button>(R.id.app_login_button)
        val registerButton = view.findViewById<Button>(R.id.app_register_button)

        spotifyButton.setOnClickListener { loginCommunicator.onSpotifyLoginButtonClick() }
        appButton.setOnClickListener { loginCommunicator.onAppLoginButtonClick() }
        registerButton.setOnClickListener { loginCommunicator.onAppRegisterButtonClick() }

        window.statusBarColor = Color.TRANSPARENT

        return view
    }

}



