package com.example.edacity.edacitymusicplayer.Module.View.Activity;

import android.app.ActivityManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.Network.API;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorSpotify;
import com.example.edacity.edacitymusicplayer.Module.Network.SpotifyAPI;
import com.example.edacity.edacitymusicplayer.Module.Model.User;
import com.example.edacity.edacitymusicplayer.Module.Service.ActionEnum;
import com.example.edacity.edacitymusicplayer.Module.Service.MediaPlayerService;
import com.example.edacity.edacitymusicplayer.Module.View.Authentication.AuthActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Browsepage.BrowseFragment;
import com.example.edacity.edacitymusicplayer.Module.View.Homepage.HomeFragment;
import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.MyLibraryFragment;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Adapter.PlaylistPlayerInteraction;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.Playpage.PlayActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.SearchFragment;
import com.example.edacity.edacitymusicplayer.Module.Utility.BottomNavigationViewHelper;
import com.example.edacity.edacitymusicplayer.R;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.player.Config;

import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements
        MainView,
        MainActivityViews,
        PlaylistPlayerInteraction{

    private final String TAG = "MainActivity";
    private final String FRAGMENT_HOME = "fragmentHome";
    private final String FRAGMENT_SEARCH = "fragmentSearch";
    private final String FRAGMENT_BROWSE = "fragmentBrowse";
    private final String FRAGMENT_MYLIBRARY = "fragmentMylibrary";
    private String accessToken, clientId, redirectUri;
    private Animation fadeOut, fadeIn;
    public static InteractorEdacity api;
    public static InteractorSpotify spotifyApi;
    private MainPresenter presenter;
    private BottomNavigationView bottomNavigationView;
    private android.widget.Toolbar toolbar;
    private View splashScreen;
    private View mainScreen;
    private Config playerConfig;
    public static final String SESSION_KEY = "edacity";
    public static final String SESSION_NAME = "user_name";
    public static final String SESSION_EMAIL = "user_email";
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    private final String COLOR_MATERIAL_GREY_DARK = "#000a12";
    private final String COLOR_MATERIAL_GREY = "#263238";
    private LinearLayout bottomNavContainer;
    private static TextView playerTitle;
    private static ImageButton buttonPlay;
    public static int shufflePlayIndex;
    private List<SptPlaylistTrack> sptPlaylistTrackList;
    private SptItemBasic sptItemBasic;
    public static boolean mPlayerState;
    public static String edacityToken;
    public static final String PLAYLIST_KEYWORD = "playlist_data_bundle";
    private static List<Playlistable> playlistableList;
    Intent serviceIntent;
    String localIntentAction = "android.intent.action.LOCAL";
    String action = "action";
    Intent broadcastIntent;
    MainActivityBroadcastReceiver mainActivityBroadcastReceiver;
    RelativeLayout playerBar;
    static boolean activityStatus = false;

    public static class MainActivityBroadcastReceiver extends BroadcastReceiver {

        private static final String TAG = "ActivityReceiver";

        public MainActivityBroadcastReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: dari service => " + intent.getAction());
            Log.d(TAG, "onReceive: dari service => " + String.valueOf(intent.getSerializableExtra("player")));
            Log.d(TAG, "onReceive: dari service => " + intent.getStringExtra("event"));
            if (Objects.equals(intent.getStringExtra("type"), "state") && activityStatus){
                switch (intent.getStringExtra("event")){
                    case "BECAME_ACTIVE":{
                        //pertama kali player dihidupkan
                        break;
                    }
                    case "TRACK_CHANGED":{
                        //jika terjadi perubahan track
                        shufflePlayIndex = intent.getIntExtra("position", 0);
                        playerTitle.setText(playlistableList.get(shufflePlayIndex).getName());
                        break;
                    }
                    case "PLAY":{
                        //track dimainkan
                        buttonPlay.setImageResource(R.drawable.icon_button_pause);
                        mPlayerState = true;
                        break;
                    }
                    case "AUDIO_FLUSH":{
                        //audio sudah menyala
                        break;
                    }
                    case "PAUSE":{
                        //pause tracks
                        buttonPlay.setImageResource(R.drawable.icon_button_play);
                        mPlayerState = false;
                        break;
                    }
                    case "END_OF_CONTEXT":{
                        //lagu selesai
                        break;
                    }
                }
            }

        }

    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setActionBar(toolbar);
        setBottomNavigationView();
        sharedPreferences = getSharedPreferences(SESSION_KEY, MODE_PRIVATE);
        accessToken = this.getIntent().getStringExtra("token");
        clientId = this.getIntent().getStringExtra("clientid");

        broadcastIntent = new Intent();
        broadcastIntent.setAction(localIntentAction);

        api = new API();
        spotifyApi = new SpotifyAPI(accessToken);
        presenter = new MainPresenter(api, spotifyApi);
        presenter.bind(this);
        edacityToken = presenter.createEdacityToken();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashScreen.setVisibility(View.GONE);
                mainScreen.bringToFront();
                bottomNavContainer.bringToFront();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                screenTransition(splashScreen, mainScreen, bottomNavContainer);
            }
        },2000);

        presenter.loadUserData();
        setFragment(new HomeFragment(), "home");

        buttonPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mPlayerState){
                            broadcastIntent.putExtra("action", ActionEnum.PAUSE);
                            sendBroadcast(broadcastIntent);
                            mPlayerState = false;
                        }else {
                            if (playlistableList!=null){
                                broadcastIntent.putExtra("action", ActionEnum.RESUME);
                                sendBroadcast(broadcastIntent);
                                mPlayerState = true;
                            }
                        }
                    }
                });

        playerBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playlistableList != null) {
                    Intent playIntent = new Intent(MainActivity.this, PlayActivity.class);
                    String playlistArray[] = new String[playlistableList.size()];
                    for (int i=0; i<playlistableList.size();i++){
                        playlistArray[i] = playlistableList.get(i).getId();
                    }
                    playIntent.putExtra("list", playlistArray);
                    playIntent.putExtra("position", shufflePlayIndex);
                    playIntent.putExtra("state", mPlayerState);
                    startActivity(playIntent);
                }
            }
        });

        serviceIntent = new Intent(getApplication(),MediaPlayerService.class);
        serviceIntent.putExtra("token", accessToken);
        serviceIntent.putExtra("clientid", clientId);
        startService(serviceIntent);
        mainActivityBroadcastReceiver = new MainActivityBroadcastReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        activityStatus = true;
        if (mPlayerState){
            buttonPlay.setImageResource(R.drawable.icon_button_pause);
            playerTitle.setText(playlistableList.get(shufflePlayIndex).getName());
        }else {
            if (playlistableList!=null){
                buttonPlay.setImageResource(R.drawable.icon_button_play);
                playerTitle.setText(playlistableList.get(shufflePlayIndex).getName());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceIntent = new Intent(getApplication(),MediaPlayerService.class);
        serviceIntent.putExtra("token", accessToken);
        serviceIntent.putExtra("clientid", clientId);
        startService(serviceIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityStatus = false;
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy() called");
//        Spotify.destroyPlayer(getApplicationContext());
//        stopService(serviceIntent);
        super.onDestroy();
    }

    private void initView(){
        fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        toolbar = findViewById(R.id.app_bar);
        bottomNavigationView = findViewById(R.id.navigation_bar);
        splashScreen = findViewById(R.id.main_splash);
        mainScreen = findViewById(R.id.main_layout);
        bottomNavContainer = findViewById(R.id.navigation_container);
        playerTitle = findViewById(R.id.player_title);
        buttonPlay = findViewById(R.id.player_button_play);
        playerBar = findViewById(R.id.player_bar);
    }

    private void setBottomNavigationView(){
        /**
         * Set Navbar, disable animasi aneh di tiap imageButton
         * @see BottomNavigationView
         */
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                /**
                 * Ketika item dari navigasi selected
                 * @param item  button yang ada di navigasi
                 * @return true jika benar terpilih
                 */
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.navbar_action_home: {
                                setFragment(new HomeFragment(), FRAGMENT_HOME);
                                break;
                            }
                            case R.id.navbar_action_browse: {
                                setFragment(new BrowseFragment(), FRAGMENT_BROWSE);
                                break;
                            }
                            case R.id.navbar_action_search: {
                                setFragment(new SearchFragment(), FRAGMENT_SEARCH);
                                break;
                            }
                            case R.id.navbar_action_library: {
                                setFragment(new MyLibraryFragment(), FRAGMENT_MYLIBRARY);
                                break;
                            }
                        }
                        return true;
                    }
                });
    }

    private void screenTransition(View splashScreen, View mainScreen, LinearLayout navbar){
        splashScreen.startAnimation(fadeOut);
        mainScreen.setVisibility(View.VISIBLE);
        navbar.setVisibility(View.VISIBLE);
        mainScreen.startAnimation(fadeIn);
        navbar.startAnimation(fadeIn);
    }

    /**
     * Option menu dibuat
     * @param menu object menu
     * @return true
     * @author Edwin
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;

    }

    /**
     * Ketika item pada menu dipilih/selected
     *
     * @param item item pada menu
     * @return true jika selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_menu_logout:
//                LoginManager.getInstance().logOut();
                AuthenticationClient.clearCookies(this);
                Intent i = new Intent(getApplicationContext(), AuthActivity.class);
                startActivity(i);
                finish();
                break;
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setFragment(Fragment fragment, String tag) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.main_fragment, fragment);
        ft.commit();
    }

    @Override
    public void userDataArrived(User user) {
        editor = sharedPreferences.edit();
        editor.putString(SESSION_NAME, user.id);
        editor.putString(SESSION_EMAIL, user.email);
        editor.apply();
    }

    @Override
    public void toolbarHasBackButton(boolean status) {

        if (status){
            if (getActionBar() != null) {
                getActionBar().setDisplayHomeAsUpEnabled(true);
                getActionBar().setHomeAsUpIndicator(R.drawable.icon_back_arrow);
            }
        }else{
            if (getActionBar() != null) {
                getActionBar().setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    @Override
    public void setTitle(String title) {
        getActionBar().setTitle(title);
    }

//    @Deprecated
//    @Override
//    public void shufflePlay(@NonNull List<Playlistable> playlistableList, SptItemBasic sptItemBasic) {
//        boolean checker = false;
//        if (this.playlistableList == null){
//            checker = true;
//        }else {
//            for (int i=0;i<this.playlistableList.size();i++){
//                if (!Objects.equals(this.playlistableList.get(i).getId(), playlistableList.get(i).getId())){
//                    checker = true;
//                    break;
//                }
//            }
//        }
//        if (checker){
//            this.playlistableList = playlistableList;
//            shufflePlayIndex = 0;
//            mPlayer.play("spotify:track:" + playlistableList.get(shufflePlayIndex).getId());
//        }
//
//    }

    /**
     * TODO: gunakan method ini untuk play Playlist
     * @param playlistableList list dari interface playlistable
     */
    @Override
    public void shufflePlay(List<Playlistable> playlistableList) {
        Log.d(TAG, "shufflePlay: ");
        boolean checker = false;
        if (this.playlistableList == null){
            checker = true;
        }else {
            for (int i=0;i<this.playlistableList.size();i++){
                if (!Objects.equals(this.playlistableList.get(i).getId(), playlistableList.get(i).getId())){
                    checker = true;
                    break;
                }
            }
        }
        if (checker){
            Log.d(TAG, "shufflePlay: sendbroadcast");
            this.playlistableList = playlistableList;
            shufflePlayIndex = 0;
            mPlayerState = true;
//            mPlayer.play("spotify:track:" + playlistableList.get(shufflePlayIndex).getId());
            String playlistArray[] = new String[this.playlistableList.size()];
            for (int i=0; i<playlistableList.size();i++){
                playlistArray[i] = playlistableList.get(i).getId();
            }
            broadcastIntent.putExtra("action", ActionEnum.PLAY);
            broadcastIntent.putExtra("list", playlistArray);
            broadcastIntent.putExtra("position", shufflePlayIndex);
            sendBroadcast(broadcastIntent);
        }
    }

    /**
     * TODO: gunakan method ini untuk play track
     * @param playlistableList list dari interface playlistable
     * @param position posisi track pada list
     */
    @Override
    public void onTrackSelected(@NonNull List<Playlistable> playlistableList, int position) {
        Log.d(TAG, "onTrackSelected: ");
//        mPlayer.clearQueue();
        this.playlistableList = playlistableList;
        shufflePlayIndex = position;
//        mPlayer.play("spotify:track:" + playlistableList.get(shufflePlayIndex).getId());
        mPlayerState = true;
        String playlistArray[] = new String[this.playlistableList.size()];
        for (int i=0; i<playlistableList.size();i++){
            playlistArray[i] = playlistableList.get(i).getId();
        }
        broadcastIntent.putExtra("action", ActionEnum.PLAY);
        broadcastIntent.putExtra("list", playlistArray);
        broadcastIntent.putExtra("position", shufflePlayIndex);
        sendBroadcast(broadcastIntent);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

/////////////////////////////////////////////////////////////////////////////////
////    STOP USING BELOW
/////////////////////////////////////////////////////////////////////////////////
//
//    @Override
//    public void onLoggedIn() {
//        Log.d(TAG, "onLoggedIn() called");
//
////        setFragment(new HomeFragment(), "home");
////        mPlayer.play("spotify:track:2TpxZ7JUBn3uw46aR7qd6V");
////        presenter.loadUserData();
//    }
//
//    @Override
//    public void onLoggedOut() {
//        Log.d(TAG, "onLoggedOut() called");
//    }
//
//    @Override
//    public void onLoginFailed(Throwable throwable) {
//        Log.d(TAG, "onLoginFailed() called with: throwable = [" + throwable + "]");
//    }
//
//    @Override
//    public void onTemporaryError() {
//        Log.d(TAG, "onTemporaryError() called");
//    }
//
//    @Override
//    public void onConnectionMessage(String s) {
//        Log.d(TAG, "onConnectionMessage() called with: s = [" + s + "]");
//    }
//
//    @Override
//    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
//        switch (eventType){
//            case BECAME_ACTIVE:{
//                //pertama kali player dihidupkan
//                break;
//            }
//            case TRACK_CHANGED:{
//                //jika terjadi perubahan track
//                playerTitle.setText(playlistableList.get(shufflePlayIndex).getName());
//                break;
//            }
//            case PLAY:{
//                //track dimainkan
//                buttonPlay.setImageResource(R.drawable.icon_button_pause);
//                mPlayerState = true;
//                break;
//            }
//            case AUDIO_FLUSH:{
//                //audio sudah menyala
//                break;
//            }
//            case PAUSE:{
//                //pause tracks
//                buttonPlay.setImageResource(R.drawable.icon_button_play);
//                mPlayerState = false;
//                break;
//            }
//            case END_OF_CONTEXT:{
//                //lagu selesai
//                shufflePlayIndex++;
//                if (playlistableList.size() > shufflePlayIndex){
//                    mPlayer.play("spotify:track:" + playlistableList.get(shufflePlayIndex).getName());
//                }else {
//                    Toast.makeText(this, "Tracks Finished", Toast.LENGTH_LONG).show();
//                }
//                break;
//            }
//        }
//
//        Log.d(TAG, "onPlaybackEvent() called with: eventType = [" + eventType + "], playerState = [" + playerState + "]");
//    }
//
//    @Override
//    public void onPlaybackError(ErrorType errorType, String s) {
//        Log.d(TAG, "onPlaybackError() called with: errorType = [" + errorType + "], s = [" + s + "]");
//    }
}