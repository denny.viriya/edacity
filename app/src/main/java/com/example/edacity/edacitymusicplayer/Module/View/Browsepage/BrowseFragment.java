package com.example.edacity.edacitymusicplayer.Module.View.Browsepage;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivityViews;
import com.example.edacity.edacitymusicplayer.R;

/**
 * Created by denny on 29/11/17.
 */

public class BrowseFragment extends Fragment {
    private MainActivityViews mainActivityViews;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivityViews = (MainActivityViews) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (mainActivityViews==null){
            mainActivityViews = (MainActivityViews) activity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.browse_fragment, container, false);
        mainActivityViews.toolbarHasBackButton(false);
        mainActivityViews.setTitle("Browse");
        return view;
    }
}
