package com.example.edacity.edacitymusicplayer.Module.Network.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by denny on 11/12/17.
 */
@Parcel
public class SptFeaturedPlaylist {
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("playlists")
    @Expose
    public List<SptPlaylist> playlists = null;
}
