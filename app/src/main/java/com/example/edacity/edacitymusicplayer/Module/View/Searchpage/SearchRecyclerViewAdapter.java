package com.example.edacity.edacitymusicplayer.Module.View.Searchpage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchAlbumResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchArtistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchPlaylistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchAlbum;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchArtist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchTrack;
import com.example.edacity.edacitymusicplayer.R;
import com.squareup.picasso.Picasso;

/**
 * Created by denny on 03/12/17.
 */

public class SearchRecyclerViewAdapter extends RecyclerView.Adapter<SearchViewHolder> {
    private SearchPresenter presenter;
    private Context context;
    private String imageUrl;
    private SptSearchPlaylistResponse playlistResponse;
    private SptSearchAlbumResponse albumResponse;
    private SptSearchTrackResponse trackResponse;
    private SptSearchArtistResponse artistResponse;
    private enum type {
        TRACK,
        PLAYLIST,
        ALBUM,
        ARTIST;
    }
    private type dataType;

    public SearchRecyclerViewAdapter(SearchPresenter presenter, Context context,
                                     SptSearchPlaylistResponse playlistResponse) {
        this.presenter = presenter;
        this.context = context;
        this.playlistResponse = playlistResponse;
        dataType = type.PLAYLIST;
    }

    public SearchRecyclerViewAdapter(SearchPresenter presenter, Context context,
                                     SptSearchAlbumResponse albumResponse) {
        this.presenter = presenter;
        this.context = context;
        this.albumResponse = albumResponse;
        dataType = type.ALBUM;
    }

    public SearchRecyclerViewAdapter(SearchPresenter presenter, Context context,
                                     SptSearchTrackResponse trackResponse) {
        this.presenter = presenter;
        this.context = context;
        this.trackResponse = trackResponse;
        dataType = type.TRACK;
    }

    public SearchRecyclerViewAdapter(SearchPresenter presenter, Context context,
                                     SptSearchArtistResponse artistResponse) {
        this.presenter = presenter;
        this.context = context;
        this.artistResponse = artistResponse;
        dataType = type.ARTIST;
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.search_listview, parent, false);
        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SearchViewHolder holder, final int position) {
        switch (dataType){
            case ALBUM:{
                SptSearchAlbum item = albumResponse.albumList.get(position);
                holder.title.setText(item.name);
                if (item.artists.size()>0){
                    holder.description.setText(albumResponse.albumList.get(position).artists.get(0).name);
                }else {
                    holder.description.setText("");
                }

                if (item.images.size()>0){
                    Glide.with(context)
                            .load(item.images.get(item.images.size()-1).url)
                            .into(holder.image);
                }else {
                    holder.image.setImageResource(R.drawable.playlist1);
                }
                break;
            }
            case TRACK:{
                SptSearchTrack item = trackResponse.trackList.get(position);
                holder.title.setText(item.name);
                if (item.artist.size()>0){
                    holder.description.setText(item.artist.get(0).name);
                }else {
                    holder.description.setText("");
                }
                holder.image.setVisibility(View.GONE);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMarginStart(24);
                params.bottomMargin = 12;
                params.topMargin = 12;
                holder.title.setLayoutParams(params);
                holder.description.setLayoutParams(params);
                break;
            }
            case ARTIST:{
                SptSearchArtist item = artistResponse.artistList.get(position);
                holder.title.setText(item.name);
                if (item.genres.size()>0){
                    holder.description.setText(item.genres.get(0));
                }else {
                    holder.description.setText("");
                }

                if (item.images.size()>0) {
                    Glide.with(context)
                            .load(item.images.get(item.images.size() - 1).url)
                            .into(holder.image);
                }else {
                    holder.image.setImageResource(R.drawable.playlist1);
                }
                break;
            }
            case PLAYLIST:{
                SptSearchPlaylist item = playlistResponse.playlistList.get(position);
                holder.title.setText(item.name);
                holder.description.setText(String.format("%s %s", item.ownerId, item.type));
                if (item.images.size()>0){
                    Glide.with(context)
                            .load(item.images.get(item.images.size()-1).url)
                            .into(holder.image);
                }else {
                    holder.image.setImageResource(R.drawable.playlist1);
                }
                break;
            }
        }

        holder.optionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context,v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.menu_add_to_playlist:{
                                return true;
                            }
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.menu_search_item);
                popupMenu.show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (dataType){
                    case ARTIST:{
                        SptSearchArtist item = artistResponse.artistList.get(position);
                        presenter.onItemSelected(item);
                        break;
                    }
                    case PLAYLIST:{
                        SptSearchPlaylist item = playlistResponse.playlistList.get(position);
                        presenter.onItemSelected(item);
                        break;
                    }
                    case TRACK:{
                        SptSearchTrack item = trackResponse.trackList.get(position);
                        presenter.onItemSelected(item);
                        break;
                    }
                    case ALBUM:{
                        SptSearchAlbum item = albumResponse.albumList.get(position);
                        presenter.onItemSelected(item);
                        break;
                    }
                    default:{
                        break;
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        switch (dataType){
            case ALBUM:{
                return albumResponse.albumList.size();
            }
            case TRACK:{
                return trackResponse.trackList.size();
            }
            case PLAYLIST:{
                return playlistResponse.playlistList.size();
            }
            case ARTIST:{
                return artistResponse.artistList.size();
            }
            default:{
                return 0;
            }
        }
    }
}

/**
 * merupakan viewHolder dari CardView yang telah dicustom
 *
 */
class SearchViewHolder extends RecyclerView.ViewHolder{

    ImageView image;
    TextView title;
    TextView description;
    ImageButton optionButton;

    SearchViewHolder(View itemView) {
        super(itemView);
        image = itemView.findViewById(R.id.search_image);
        title = itemView.findViewById(R.id.search_title);
        description = itemView.findViewById(R.id.search_explanation);
        optionButton = itemView.findViewById(R.id.list_button);
    }
}
