package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivityViews;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Adapter.PlaylistPlayerInteraction;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Adapter.PlaylistGenericRVAdapter;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistAlbumTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistArtistTrackResponse;
import com.example.edacity.edacitymusicplayer.R;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;


public class PlaylistFragment extends Fragment implements PlaylistPageView {
    private static final String TAG = "PLAYLIST FRAGMENT";
    private ImageView playlistImage;
    private TextView playlistTitle, playlistMessage;
    private Button playlistPlayButton;
    private Bundle playlistDataForFragment;
//    private SptPlaylist sptPlaylist;
    private MainActivityViews mainActivityViews;
    public static PlaylistPagePresenter presenter;
    private InteractorEdacity api;
    private RecyclerView tracksRecyclerView;
    private List<SptPlaylistTrack> sptPlaylistTrackList;
    private PlaylistPlayerInteraction playerInteraction;
    private SptItemBasic initialData;
    private SptPlaylistAlbumTrackResponse albumTrackResponse;
    private SptPlaylistArtistTrackResponse artistTrackResponse;
    private PlaylistGenericRVAdapter<List<Playlistable>> playlistGenericRVAdapter;
    private List<Playlistable> playlistableList;

    public PlaylistFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivityViews = (MainActivityViews) context;
        playerInteraction = (PlaylistPlayerInteraction) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (mainActivityViews==null){
            mainActivityViews = (MainActivityViews) activity;
            playerInteraction = (PlaylistPlayerInteraction) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playlistDataForFragment = getArguments();
        initialData = Parcels.unwrap(playlistDataForFragment.getParcelable(MainActivity.PLAYLIST_KEYWORD));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.playlist_fragment, container, false);
        initView(view);

        api = MainActivity.api;
        presenter = new PlaylistPagePresenter(api);
        presenter.bind(this);

        mainActivityViews.setTitle("Playlist");
        mainActivityViews.toolbarHasBackButton(true);

        if (initialData.imageUrl.isEmpty()){
            playlistImage.setImageResource(R.drawable.playlist1);
        }else {
            Glide.with(this).load(initialData.imageUrl).into(playlistImage);
        }
        playlistTitle.setText(initialData.name);
        playlistMessage.setText(initialData.id);
        playlistPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playlistableList!=null){
                    if (!playlistableList.isEmpty()){
                        mainActivityViews.shufflePlay(playlistableList);
                        Log.d(TAG, "onClick: clicked");
                    }
                }
            }
        });

        presenter.loadPlaylistTracks(initialData);

        return view;
    }

    private void initView(View view){
        playlistImage = view.findViewById(R.id.playlist_image_imageview);
        playlistTitle = view.findViewById(R.id.playlist_title_textview);
        playlistMessage = view.findViewById(R.id.playlist_message_textview);
        playlistPlayButton = view.findViewById(R.id.playlist_play_button);
        tracksRecyclerView = view.findViewById(R.id.playlist_tracks_recyclerview);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unBind();
    }

    @Override
    public void updateUi(List<SptPlaylistTrack> sptPlaylistTrackList) {
        this.sptPlaylistTrackList = sptPlaylistTrackList;
        tracksRecyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false)
        );
        tracksRecyclerView.setNestedScrollingEnabled(false);

        playlistableList = new ArrayList<>();
        playlistableList.addAll(sptPlaylistTrackList);

        playlistGenericRVAdapter = new PlaylistGenericRVAdapter<>(playlistableList, ApiType.PLAYLIST);
        playlistGenericRVAdapter.setFragmentManager(getFragmentManager());
        tracksRecyclerView.setAdapter(playlistGenericRVAdapter);

    }

    @Override
    public void updateUi(SptPlaylistAlbumTrackResponse albumTrackResponse) {
        this.albumTrackResponse = albumTrackResponse;
        tracksRecyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false)
        );
        tracksRecyclerView.setNestedScrollingEnabled(false);

        playlistableList = new ArrayList<>();
        playlistableList.addAll(albumTrackResponse.items);

        playlistGenericRVAdapter = new PlaylistGenericRVAdapter<>(playlistableList, ApiType.ALBUM);
        playlistGenericRVAdapter.setFragmentManager(getFragmentManager());
        tracksRecyclerView.setAdapter(playlistGenericRVAdapter);
    }

    @Override
    public void updateUi(SptPlaylistArtistTrackResponse artistTrackResponse) {
        this.artistTrackResponse = artistTrackResponse;
        tracksRecyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false)
        );
        tracksRecyclerView.setNestedScrollingEnabled(false);

        playlistableList = new ArrayList<>();
        playlistableList.addAll(artistTrackResponse.items);

        playlistGenericRVAdapter = new PlaylistGenericRVAdapter<>(playlistableList, ApiType.ARTIST);
        playlistGenericRVAdapter.setFragmentManager(getFragmentManager());
        tracksRecyclerView.setAdapter(playlistGenericRVAdapter);
    }

    @Override
    public void onItemSelected(List<Playlistable> playlistableList, int position) {
        playerInteraction.onTrackSelected(playlistableList, position);
    }

}
