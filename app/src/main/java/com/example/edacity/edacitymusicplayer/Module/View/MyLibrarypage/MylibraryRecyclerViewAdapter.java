package com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model.UserPlaylistData;
import com.example.edacity.edacitymusicplayer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denny on 11/12/17.
 */

public class MylibraryRecyclerViewAdapter extends RecyclerView.Adapter<PlaylistViewHolder>{
    private UserPlaylistData userPlaylist;
    private MyLibraryPresenter presenter;

    public MylibraryRecyclerViewAdapter(UserPlaylistData userPlaylist) {
        this.userPlaylist = userPlaylist;
        initPresenter();
    }

    public void updateDataset(UserPlaylistData userPlaylist){
        this.userPlaylist = userPlaylist;
    }

    private void initPresenter(){
        presenter = MyLibraryFragment.presenter;
    }

    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_library_playlist_viewholder, parent, false);
        return new PlaylistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaylistViewHolder holder, final int position) {
        holder.playlistName.setText(userPlaylist.playlists.get(position).playlistName);

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.deletePlaylist(userPlaylist, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return userPlaylist.playlists.size();
    }
}

class PlaylistViewHolder extends RecyclerView.ViewHolder{
    TextView playlistName;
    ImageButton deleteButton;

    PlaylistViewHolder(View itemView) {
        super(itemView);
        playlistName = itemView.findViewById(R.id.mylibrary_user_playlist);
        deleteButton = itemView.findViewById(R.id.my_library_delete_playlist);
    }
}