package com.example.edacity.edacitymusicplayer.Module.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.edacity.edacitymusicplayer.Module.Network.API;
import com.example.edacity.edacitymusicplayer.Module.Service.Model.TrackInfo;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.R;
import com.google.gson.reflect.TypeToken;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.PlayerStateCallback;
import com.spotify.sdk.android.player.Spotify;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denny on 07/01/18.
 */

public class MediaPlayerService extends Service implements PlayerNotificationCallback, ConnectionStateCallback{
    private static final String TAG = "Service";
    public Player sPlayer;
    private List<Playlistable> playlistableList;
    private static int position;
    private Looper mServiceLooper;
    private PlayerConfigHandler mPlayerConfigHandler;
    static NotificationCompat.Builder notifBuilder;
    private PendingIntent contentIntent;
    static NotificationManager notificationManager;
    private Message message;
    private HandlerThread handlerThread;
    private static int notifId = 1;
    private Config playerConfig;
    private String accessToken, clientId;
    static boolean mPlayerState;
    static Player mPlayer;
    private String channelId = "Channel_Notif_1";
    private ServiceBroadcastReceiver serviceBroadcastReceiver;
    static String[] playlistArray;
    private API api;
    private Intent reOpenIntent;
    static TrackInfo trackInfo;
    static Intent broadcastIntent;
    private String remoteIntentAction = "android.intent.action.REMOTE";
    private String localIntentAction = "android.intent.action.LOCAL";
    static Context appContext;
    static Bitmap defaultImage;
    private Intent prevIntent;
    private PendingIntent pendingIntentPrev;
    private Intent playIntent;
    private PendingIntent pendingIntentPlay;
    private Intent nextIntent;
    private PendingIntent pendingIntentNext;
    private static String sToken, sClientId;

    private final class PlayerConfigHandler extends Handler{

        public PlayerConfigHandler(Looper looper){
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            playerConfig = new Config(getApplicationContext(), accessToken, clientId);
            Spotify.getPlayer(playerConfig, getApplicationContext(), new Player.InitializationObserver() {
                @Override
                public void onInitialized(Player player) {
                    Log.d(TAG, "onInitialized: player initialized, adding Callback...");
                    mPlayerState = false;
                    mPlayer = player;
                    mPlayer.addConnectionStateCallback(MediaPlayerService.this);
                    mPlayer.addPlayerNotificationCallback(MediaPlayerService.this);
                }

                @Override
                public void onError(Throwable throwable) {
                    Log.e(TAG, "onError: cant initialize player" + throwable.getMessage());
                }
            });
        }
    }

    /**
     * Handler Thread untuk config player Spotify
     * Asynchronous thread
     */
    public static class ServiceBroadcastReceiver extends BroadcastReceiver {

        private static final String TAG = "MediaBroadcastReceiver";

        public ServiceBroadcastReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: menerima broadcast => " + intent.getAction());



            /*
             *  Setelah menerima broadcast dari aplikasi, maka kirimkan lagi broadcast ke aplikasi
             *  dengan pesan yang sama persis.
             *  agar aplikasi tahu bahwa service masih hidup
             */
            if (intent.getSerializableExtra("action")!=null) {

                switch ((ActionEnum) intent.getSerializableExtra("action")) {
                    case PLAY: {
                        Log.d(TAG, "onReceive: PLAY");
                        playlistArray = intent.getStringArrayExtra("list");
                        position = intent.getIntExtra("position", 0);
                        mPlayer.play("spotify:track:" + playlistArray[position]);

                        broadcastIntent.putExtra("type", "control");
                        broadcastIntent.putExtra("player", ActionEnum.PLAY);
                        context.sendBroadcast(broadcastIntent);
                        break;
                    }
                    case PAUSE: {
                        mPlayer.pause();

                        broadcastIntent.putExtra("type", "control");
                        broadcastIntent.putExtra("player", ActionEnum.PAUSE);
                        context.sendBroadcast(broadcastIntent);
                        break;
                    }
                    case RESUME: {
                        mPlayer.resume();
                        broadcastIntent.putExtra("type", "control");
                        broadcastIntent.putExtra("player", ActionEnum.RESUME);
                        context.sendBroadcast(broadcastIntent);
                        break;
                    }
                    case NEXT: {
                        mPlayer.clearQueue();
                        position = position + 1;
                        mPlayer.play("spotify:track:" + playlistArray[position]);
                        broadcastIntent.putExtra("type", "control");
                        broadcastIntent.putExtra("player", ActionEnum.NEXT);
                        context.sendBroadcast(broadcastIntent);
                        break;
                    }
                    case PREV: {
                        mPlayer.clearQueue();
                        position = position - 1;
                        mPlayer.play("spotify:track:" + playlistArray[position]);
                        broadcastIntent.putExtra("type", "control");
                        broadcastIntent.putExtra("player", ActionEnum.PREV);
                        context.sendBroadcast(broadcastIntent);
                        break;
                    }
                    case STOP: {
                        broadcastIntent.putExtra("type", "control");
                        broadcastIntent.putExtra("player", ActionEnum.STOP);
                        context.sendBroadcast(broadcastIntent);
                        break;
                    }
                    default: {
                        Log.d(TAG, "onReceive: default");
                        break;
                    }
                }
            }else if (intent.getIntExtra("click", 0) != 0){
                if (playlistArray!=null) {

                    switch (intent.getIntExtra("click", 0)) {
                        case 1: {
                            //prev

                            mPlayer.clearQueue();
                            position = position - 1;
                            mPlayer.play("spotify:track:" + playlistArray[position]);
                            broadcastIntent.putExtra("type", "control");
                            broadcastIntent.putExtra("player", ActionEnum.PREV);
                            context.sendBroadcast(broadcastIntent);
                            break;
                        }
                        case 2: {
                            //play jika sudah ada list lagu di service
                            if (mPlayerState) {
                                //sedang play musik
                                mPlayer.pause();
                                broadcastIntent.putExtra("type", "control");
                                broadcastIntent.putExtra("player", ActionEnum.PAUSE);
                                context.sendBroadcast(broadcastIntent);
                            } else {
                                //sedang berhenti
                                mPlayer.resume();
                                broadcastIntent.putExtra("type", "control");
                                broadcastIntent.putExtra("player", ActionEnum.RESUME);
                                context.sendBroadcast(broadcastIntent);
                            }
                            break;
                        }
                        case 3: {
                            //next
                            mPlayer.clearQueue();
                            position = position + 1;
                            mPlayer.play("spotify:track:" + playlistArray[position]);
                            broadcastIntent.putExtra("type", "control");
                            broadcastIntent.putExtra("player", ActionEnum.NEXT);
                            context.sendBroadcast(broadcastIntent);
                            break;
                        }
                    }
                }
            }else if (intent.getBooleanExtra("auth_act", false)){
                broadcastIntent.putExtra("auth_service", true);
                broadcastIntent.putExtra("token", sToken );
                broadcastIntent.putExtra("clientid", sClientId );
                context.sendBroadcast(broadcastIntent);
            }
        }

    }

    /**
     * dipanggil hanya sekali pada saat oncreate activity
     */
    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: ");
        appContext = getApplicationContext();
        api = new API();
        serviceBroadcastReceiver = new ServiceBroadcastReceiver();
        defaultImage = BitmapFactory.decodeResource(getResources(), R.drawable.playlist1);

        handlerThread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_FOREGROUND);
        handlerThread.start();
        mServiceLooper = handlerThread.getLooper();
        mPlayerConfigHandler = new PlayerConfigHandler(mServiceLooper);

        broadcastIntent = new Intent();
        broadcastIntent.setAction(remoteIntentAction);

        initNotification();
    }

    /**
     * membuat Notification builder yang nantinya akan diupdate terus
     * membuat Notification manager untuk notify update
     * @see NotificationCompat.Builder
     * @see NotificationManager
     */
    private void initNotification(){

        prevIntent = new Intent();
        prevIntent.setAction("android.intent.action.PREV");
        prevIntent.putExtra("click", 1);
        pendingIntentPrev = PendingIntent.getBroadcast(this,
                9915, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        playIntent = new Intent();
        playIntent.setAction("android.intent.action.PLAY");
        playIntent.putExtra("click", 2);
        pendingIntentPlay = PendingIntent.getBroadcast(this,
                9915, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        nextIntent = new Intent();
        nextIntent.setAction("android.intent.action.NEXT");
        nextIntent.putExtra("click", 3);
        pendingIntentNext = PendingIntent.getBroadcast(this,
                9915, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        notifBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setSmallIcon(R.drawable.playlist1)
                .setContentTitle("Stop LDB")
                .setContentText("Click to stop LDB")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.playlist1))
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle())
                .addAction(R.drawable.icon_player_previous, "PREV", pendingIntentPrev)
                .addAction(R.drawable.icon_player_play, "PLAY", pendingIntentPlay)
                .addAction(R.drawable.icon_player_next, "NEXT", pendingIntentNext)
                .setOngoing(false);
        reOpenIntent = new Intent(this, MainActivity.class);
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    /**
     * dipanggil ketika
     * 1. oncreate activity (ketika service running maupun tidak)
     * 2. notification clicked
     * @param intent intent yang dikirim dari activity
     * @param flags
     * @param startId
     * @return STICKY: tidak bisa didestroy dengan swipe
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
        Toast.makeText(this, "Service Starting", Toast.LENGTH_SHORT).show();
        if (intent!=null){
            if (intent.getStringExtra("token") != null && !Objects.equals(intent.getStringExtra("token"), accessToken)){
                accessToken = intent.getStringExtra("token");
                clientId = intent.getStringExtra("clientid");
                message = mPlayerConfigHandler.obtainMessage();
                message.arg1 = startId;
                mPlayerConfigHandler.sendMessage(message);

                sToken = accessToken;
                sClientId = clientId;
            }
        }
        startNotification(accessToken, clientId);
        return START_STICKY;
    }

    /**
     * Memulai mengisi notification dengan data agar bisa membuka Main activity dengan lancar
     * @param accessToken access token spotify
     * @param clientId client id spotify
     */
    private void startNotification(String accessToken, String clientId){
        reOpenIntent.putExtra("token", accessToken);
        reOpenIntent.putExtra("clientid", clientId);
        contentIntent = PendingIntent.getActivity(this, 0,
                reOpenIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notifBuilder.setContentIntent(contentIntent);
        notificationManager.notify(notifId, notifBuilder.build());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: service destroyed");
        Toast.makeText(this, "Service Done", Toast.LENGTH_SHORT).show();
        Spotify.destroyPlayer(getApplicationContext());
        stopSelf();
        notificationManager.cancelAll();
        getApplication().unregisterReceiver(serviceBroadcastReceiver);
    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
        Log.d(TAG, "onPlaybackEvent: " + String.valueOf(eventType));
        switch (eventType){
            case BECAME_ACTIVE:{
                //pertama kali player dihidupkan
                sendBroadcast(new Intent()
                        .setAction(remoteIntentAction)
                        .putExtra("event", "BECAME_ACTIVE"));
                break;
            }
            case TRACK_CHANGED:{
                //jika terjadi perubahan track
                Call<TrackInfo> trackInfoCall = api.getTrackInfo(playlistArray[position]);
                trackInfoCall.enqueue(new Callback<TrackInfo>() {
                    @Override
                    public void onResponse(Call<TrackInfo> call, Response<TrackInfo> response) {
                        trackInfo = response.body();
                        notifBuilder
                                .setContentTitle(trackInfo.getName())
                                .setContentText(trackInfo.getArtists().get(0).getArtistName());

                        notificationManager.notify(notifId, notifBuilder.build());
                        new ImageLoaderTask().execute();
                    }

                    @Override
                    public void onFailure(Call<TrackInfo> call, Throwable t) {

                    }
                });

                broadcastIntent.putExtra("type", "state")
                        .putExtra("event", "TRACK_CHANGED")
                        .putExtra("position", position);
                sendBroadcast(broadcastIntent);
                break;
            }
            case PLAY:{
                //track dimainkan
                mPlayerState = true;
                broadcastIntent.putExtra("type", "state")
                        .putExtra("event","PLAY");
                sendBroadcast(broadcastIntent);
                notifBuilder.mActions.get(1).icon = R.drawable.icon_player_pause;

                notificationManager.notify(notifId, notifBuilder.build());
                startForeground(notifId, notifBuilder.build());
                break;
            }
            case AUDIO_FLUSH:{
                //audio sudah menyala
                broadcastIntent.putExtra("type", "state")
                        .putExtra("event", "AUDIO_FLUSH");
                sendBroadcast(broadcastIntent);
                break;
            }
            case PAUSE:{
                //pause tracks
                mPlayerState = false;
                broadcastIntent.putExtra("type", "state")
                        .putExtra("event","PAUSE");
                sendBroadcast(broadcastIntent);
                notifBuilder.mActions.get(1).icon = R.drawable.icon_player_play;

                notificationManager.notify(notifId, notifBuilder.build());
                stopForeground(false);

                break;
            }
            case END_OF_CONTEXT:{
                //lagu selesai
                position++;
                if (playlistArray.length > position){
                    mPlayer.play("spotify:track:" + playlistArray[position]);
                }else {
                    Toast.makeText(this, "Tracks Finished", Toast.LENGTH_LONG).show();
                }
                broadcastIntent.putExtra("type", "state")
                        .putExtra("event", "END_OF_CONTEXT");

                sendBroadcast(broadcastIntent);
                break;
            }
        }//switch end

    }//method end

    /**
     * Background task untuk load image
     */
    private static final class ImageLoaderTask extends AsyncTask<TrackInfo, Void, Bitmap>{
        Bitmap bitmap;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bitmap = null;
        }

        @Override
        protected Bitmap doInBackground(TrackInfo... trackInfos) {
            try {
                bitmap = Glide
                        .with(appContext)
                        .asBitmap()
                        .load(getOptimumImage(trackInfo))
                        .submit(100,100)
                        .get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap == null){
                notifBuilder.setLargeIcon(defaultImage);
            }else {
                notifBuilder.setLargeIcon(bitmap);
            }
            notificationManager.notify(notifId, notifBuilder.build());
        }
    }

    /**
     * memilah image terbaik untuk glide
     * @param trackInfo data track dari spotify
     * @return String URL dengan resolusi optimum
     */
    private static String getOptimumImage(TrackInfo trackInfo){
        if (trackInfo != null){
            if (trackInfo.getAlbumImages() != null){
                if (trackInfo.getAlbumImages().size()>1){
                    return trackInfo.getAlbumImages().get(trackInfo.getAlbumImages().size()-1).getUrl();
                }
                else {
                    return trackInfo.getAlbumImages().get(0).getUrl();
                }
            }
        }
        return null;
    }

    @Override
    public void onPlaybackError(ErrorType errorType, String s) {
        Log.d(TAG, "onPlaybackError: ");
    }

    @Override
    public void onLoggedIn() {
        Log.d(TAG, "onLoggedIn: ");
    }

    @Override
    public void onLoggedOut() {
        Log.d(TAG, "onLoggedOut: ");
    }

    @Override
    public void onLoginFailed(Throwable throwable) {
        Log.d(TAG, "onLoginFailed: ");
    }

    @Override
    public void onTemporaryError() {
        Log.d(TAG, "onTemporaryError: ");
    }

    @Override
    public void onConnectionMessage(String s) {
        Log.d(TAG, "onConnectionMessage: ");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

