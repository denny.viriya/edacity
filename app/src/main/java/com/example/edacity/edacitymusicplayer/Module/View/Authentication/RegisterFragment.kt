package com.example.edacity.edacitymusicplayer.Module.View.Authentication


import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup

import com.example.edacity.edacitymusicplayer.R

/**
 * Register user
 * @author Denny
 */
class RegisterFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_register, container, false)
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        toolbar.setBackgroundColor(Color.DKGRAY)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity!!.window?.statusBarColor = Color.DKGRAY

        return view
    }



}
