package com.example.edacity.edacitymusicplayer.Module.View.Playpage;

import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.Service.Model.TrackInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denny on 10/01/18.
 */

public class TrackSubstancePresenter {
    private InteractorEdacity api;
    private TrackSubstanceView view;
    private TrackInfo trackInfo;

    public TrackSubstancePresenter(InteractorEdacity api) {
        this.api = api;
    }

    public void bind(TrackSubstanceView view){
        this.view = view;
    }

    public void unBind(){
        this.view = null;
    }

    public void getTrackInfo(String id){
        Call<TrackInfo> trackInfoCall = api.getTrackInfo(id);
        trackInfoCall.enqueue(new Callback<TrackInfo>() {
            @Override
            public void onResponse(Call<TrackInfo> call, Response<TrackInfo> response) {
                trackInfo = response.body();
                view.onDataArrived(trackInfo);


            }

            @Override
            public void onFailure(Call<TrackInfo> call, Throwable t) {

            }
        });
    }

}
