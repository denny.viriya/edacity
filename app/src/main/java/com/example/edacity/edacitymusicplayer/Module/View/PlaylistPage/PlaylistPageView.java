package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage;

import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistArtist;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistAlbumTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response.SptPlaylistArtistTrackResponse;

import java.util.List;

/**
 * Created by denny on 23/12/17.
 */

public interface PlaylistPageView {
    void updateUi(List<SptPlaylistTrack> sptPlaylistTrackList);
    void updateUi(SptPlaylistAlbumTrackResponse albumTrackResponse);
    void updateUi(SptPlaylistArtistTrackResponse artistTrackResponse);

    /**
     * dipanggil oleh presenter
     * @see PlaylistPagePresenter
     * @param playlistableList
     * @param position
     */
    void onItemSelected(List<Playlistable> playlistableList, int position);
}
