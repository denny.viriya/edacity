package com.example.edacity.edacitymusicplayer.Module.Utility;

import android.util.Log;

import com.google.gson.Gson;

import java.util.logging.Logger;

/**
 * Created by denny on 08/11/17.
 */

public class CustomLogger {
    private Gson gson = new Gson();

    public CustomLogger(){
    //empty constructor
    }

    public void logArray(Object object){
        String json = gson.toJson(object);
        int length = json.length();
        StringBuilder acc = new StringBuilder();
        for(int i=0; i<length; i++)
        {
            acc.append(json.charAt(i));
            if (json.charAt(i) == ','){
                Log.i("JSON", acc.toString());
                acc = new StringBuilder();
            }
        }
    }


}
