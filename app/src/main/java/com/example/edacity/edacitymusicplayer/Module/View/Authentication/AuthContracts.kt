package com.example.edacity.edacitymusicplayer.Module.View.Authentication

interface AuthContracts {

    /**
     * Interactor untuk auth activity
     */
    interface IAuthActivity {
        fun refreshAccessToken()
    }

    /**
     * Interactor untuk auth presenter
     */
    interface  IAuthPresenter

    /**
     * Communicator untuk login fragment memanggil activity
     */
    interface LoginFragmentCommunicator {
        fun onSpotifyLoginButtonClick()
        fun onAppLoginButtonClick()
        fun onAppRegisterButtonClick()
    }
}