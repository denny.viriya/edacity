package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Response;

import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Artist.SptPlaylistArtistItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 11/01/18.
 */

public class SptPlaylistArtistTrackResponse {
    @SerializedName("items")
    @Expose
    public List<SptPlaylistArtistItem> items = null;
}
