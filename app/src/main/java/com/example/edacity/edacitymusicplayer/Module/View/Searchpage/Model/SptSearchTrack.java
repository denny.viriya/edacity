package com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.Model.SptItem;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by denny on 01/01/18.
 */
@Parcel
public class SptSearchTrack extends SptItem implements Playlistable{
    @SerializedName("popularity")
    @Expose
    public Integer popularity;

    @SerializedName("artist")
    @Expose
    public List<SptSearchArtist> artist = null;

    @SerializedName("uri")
    @Expose
    public String uri;

    public ApiType apiType;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription1() {
        if (artist!=null){
            if (artist.size()>0){
                StringBuilder s = new StringBuilder();
                for (SptSearchArtist item: artist) {
                    s.append(item.name);
                    s.append(" ");
                }
                return s.toString();
            }else {
                return "-";
            }
        }else{
            return "-";
        }
    }

    @Override
    public String getDescription2() {
        return "popularity: " + popularity;
    }

    @Override
    public ApiType getApiType() {
        return apiType;
    }
}
