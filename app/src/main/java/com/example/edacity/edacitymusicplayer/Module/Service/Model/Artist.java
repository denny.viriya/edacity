package com.example.edacity.edacitymusicplayer.Module.Service.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denny on 10/01/18.
 */

public class Artist {
    @SerializedName("artist_id")
    @Expose
    private String artistId;
    @SerializedName("artist_name")
    @Expose
    private String artistName;
    @SerializedName("artist_uri")
    @Expose
    private String artistUri;

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistUri() {
        return artistUri;
    }

    public void setArtistUri(String artistUri) {
        this.artistUri = artistUri;
    }
}
