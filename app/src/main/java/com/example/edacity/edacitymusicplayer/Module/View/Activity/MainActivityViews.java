package com.example.edacity.edacitymusicplayer.Module.View.Activity;

import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylistTrack;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;

import java.util.List;

/**
 * Created by denny on 12/12/17.
 * digunakan oleh class lain, untuk berinteraksi dengan UI dari
 * @see MainActivity
 */

public interface MainActivityViews {
    /**
     * set agar activity punya back button
     * @param status true atau false
     */
    void toolbarHasBackButton(boolean status);

    /**
     * set title pada toolbar
     * @param title judul toolbar
     */
    void setTitle(String title);

//    /**
//     * method lama, jangan digunakan
//     * @param playlistableList
//     * @param sptItemBasic
//     */
//    @Deprecated
//    void shufflePlay(List<Playlistable> playlistableList, SptItemBasic sptItemBasic);

    /**
     * TODO: gunakan ini sebagai method utama play playlist
     * @param playlistableList list dari interface playlistable
     */
    void shufflePlay(List<Playlistable> playlistableList);

}
