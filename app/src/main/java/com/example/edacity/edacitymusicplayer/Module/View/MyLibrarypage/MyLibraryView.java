package com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage;

import com.example.edacity.edacitymusicplayer.Module.Model.User;
import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model.UserPlaylistData;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by denny on 10/12/17.
 */

public interface MyLibraryView {
    void updateUserUi(User user);
    void updatePlaylistUi(UserPlaylistData userPlaylist);
}