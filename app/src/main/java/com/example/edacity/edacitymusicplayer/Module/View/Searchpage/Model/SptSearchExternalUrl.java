package com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by denny on 01/01/18.
 */
@Parcel
public class SptSearchExternalUrl {
    @SerializedName("spotify")
    @Expose
    public String spotify;
}
