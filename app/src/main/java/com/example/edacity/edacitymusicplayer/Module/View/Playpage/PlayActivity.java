package com.example.edacity.edacitymusicplayer.Module.View.Playpage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.edacity.edacitymusicplayer.Module.Service.ActionEnum;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.R;

import java.lang.ref.WeakReference;
import java.util.Objects;

public class PlayActivity extends AppCompatActivity {
    private static ViewPager viewPager;
    private PlayCollectionPagerAdapter adapter;
    private Bundle bundle;
    private ImageButton nextButton, prevButton;
    private static ImageButton playButton;
    private static boolean playerState;
    private static int playerPosition;
    String localIntentAction = "android.intent.action.LOCAL";
    PlayActivityBroadcastReceiver broadcastReceiver;
    Intent broadcastIntent;
    static boolean activityStatus = false;
    String[] tracks;

    public static class PlayActivityBroadcastReceiver extends BroadcastReceiver {

        private static final String TAG = "PlayActivityReceiver";

        public PlayActivityBroadcastReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Objects.equals(intent.getStringExtra("type"), "state") && activityStatus){
                switch (intent.getStringExtra("event")){
                    case "BECAME_ACTIVE":{
                        //pertama kali player dihidupkan
                        break;
                    }
                    case "TRACK_CHANGED":{
                        //jika terjadi perubahan track
                        playerPosition = intent.getIntExtra("position", 0);
                        viewPager.setCurrentItem(playerPosition);
                        break;
                    }
                    case "PLAY":{
                        //track dimainkan
                        playButton.setImageResource(R.drawable.icon_player_pause);
                        playerState = true;
                        break;
                    }
                    case "AUDIO_FLUSH":{
                        //audio sudah menyala
                        break;
                    }
                    case "PAUSE":{
                        //pause tracks
                        playButton.setImageResource(R.drawable.icon_player_play);
                        playerState = false;
                        break;
                    }
                    case "END_OF_CONTEXT":{
                        //lagu selesai
                        break;
                    }
                }
            }

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        bundle = getIntent().getExtras();
        initView();


        playerState = bundle.getBoolean("state");
        playerPosition = bundle.getInt("position");
        tracks = bundle.getStringArray("list");

        if (playerState){
            playButton.setImageResource(R.drawable.icon_player_pause);
        }else {
            playButton.setImageResource(R.drawable.icon_player_play);
        }

        adapter = new PlayCollectionPagerAdapter(getSupportFragmentManager());
        adapter.setData(tracks, playerPosition);
        viewPager.setAdapter(adapter);
        setupViewPager();

        setupReceiver();
        setButtonListener();
        setupPageChangeListener();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.mPlayerState = playerState;
        MainActivity.shufflePlayIndex = playerPosition;
    }

    private void initView(){
        viewPager = findViewById(R.id.pager) ;
        nextButton = findViewById(R.id.player_next);
        prevButton = findViewById(R.id.player_previous);
        playButton = findViewById(R.id.player_play);
    }

    private void setButtonListener(){
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerPosition < tracks.length){
                    viewPager.setCurrentItem(playerPosition+1);
                }
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerPosition > 0){
                    viewPager.setCurrentItem(playerPosition-1);
                }
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerState){
                    //Sedang memainkan musik (Playing)
                    playerState = false;
                    playButton.setImageResource(R.drawable.icon_player_play);

                    broadcastIntent.putExtra("action", ActionEnum.PAUSE);
                    sendBroadcast(broadcastIntent);
                }else {
                    //tidak sedang memainkan musik (paused)
                    playerState = true;
                    playButton.setImageResource(R.drawable.icon_player_pause);

                    broadcastIntent.putExtra("action", ActionEnum.RESUME);
                    sendBroadcast(broadcastIntent);
                }
            }
        });

    }

    private void setupViewPager(){
        viewPager.setCurrentItem(playerPosition);
        viewPager.setClipToPadding(false);
        viewPager.setPadding(20,0,20,0);
        viewPager.setPageMargin(-70);
        viewPager.setOffscreenPageLimit(12);
    }

    private void setupReceiver(){
        broadcastIntent = new Intent();
        broadcastIntent.setAction(localIntentAction);
        broadcastReceiver = new PlayActivityBroadcastReceiver();
    }

    private void setupPageChangeListener(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (playerPosition<position){
                    broadcastIntent.putExtra("action", ActionEnum.NEXT);
                    sendBroadcast(broadcastIntent);
                }else if (playerPosition>position){
                    broadcastIntent.putExtra("action", ActionEnum.PREV);
                    sendBroadcast(broadcastIntent);
                }
                playerPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityStatus = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityStatus = false;
    }
}
