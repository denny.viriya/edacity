package com.example.edacity.edacitymusicplayer.Module.View.Playpage;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.edacity.edacitymusicplayer.Module.Network.API;
import com.example.edacity.edacitymusicplayer.Module.Service.Model.TrackInfo;
import com.example.edacity.edacitymusicplayer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrackSubstanceFragment extends Fragment implements TrackSubstanceView{
    ImageView trackImage;
    TextView trackName, trackDesc, trackDesc2;
    TrackSubstancePresenter presenter;
    API api;
    Bundle args;
    String id;
    TrackInfo trackInfo;

    public static final String ARG_OBJECT = "object";

    public TrackSubstanceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = new API();
        presenter = new TrackSubstancePresenter(api);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_track_substance, container, false);
        args = getArguments();
        id = args.getString("id");
        initView(view);

        presenter.bind(this);
        presenter.getTrackInfo(id);

        return view;
    }

    private void initView(View view){
        trackName = view.findViewById(R.id.track_name);
        trackName.setText("Track name");
        trackDesc = view.findViewById(R.id.track_desc);
        trackDesc.setText("Track Artist");
        trackDesc2 = view.findViewById(R.id.track_desc2);
        trackDesc2.setText("Track Album");
        trackImage = view.findViewById(R.id.track_image);
        trackImage.setImageResource(R.drawable.playlist1);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unBind();
    }

    @Override
    public void onDataArrived(TrackInfo trackInfo) {
        this.trackInfo = trackInfo;
        if (this.trackInfo != null){
            trackName.setText(this.trackInfo.getName());

            if (this.trackInfo.getArtists()!=null){
                trackDesc.setText(this.trackInfo.getArtists().get(0).getArtistName());
            }else {
                trackDesc.setText("Artist Unknown");
            }

            if (this.trackInfo.getAlbumName()!=null){
                trackDesc2.setText(this.trackInfo.getAlbumName());
            }else trackDesc2.setText("Album Unknown");

            if (this.trackInfo.getAlbumImages()!=null){
                Glide.with(this).load(trackInfo.getAlbumImages().get(0).getUrl()).into(trackImage);
            }
        }
    }
}
