package com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response;

import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchArtist;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by denny on 01/01/18.
 */

public class SptSearchArtistResponse {
    @SerializedName("items")
    @Expose
    public List<SptSearchArtist> artistList = null;
}
