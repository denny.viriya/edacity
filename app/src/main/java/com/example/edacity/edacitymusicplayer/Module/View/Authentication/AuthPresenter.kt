package com.example.edacity.edacitymusicplayer.Module.View.Authentication

import android.app.Activity
import android.content.Intent

import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse

import com.spotify.sdk.android.authentication.LoginActivity.REQUEST_CODE

/**
 * Created by denny on 10/12/17.
 */

class AuthPresenter : AuthContracts.IAuthPresenter {
    private val CLIENT_ID = "396415bf72d1417aaaf35e356ebdeec7"
    private val REDIRECT_URI = "edacity-android://callback"

    private var view: AuthContracts.IAuthActivity? = null
    private val builder: AuthenticationRequest.Builder
    private lateinit var request: AuthenticationRequest
    private lateinit var authenticationResponse: AuthenticationResponse

    var accessToken: String? = null
        private set

    init {
        builder = AuthenticationRequest.Builder(CLIENT_ID,
                AuthenticationResponse.Type.TOKEN,
                REDIRECT_URI)
        builder.setScopes(arrayOf("user-read-email", "streaming"))
    }

    fun bind(view: AuthContracts.IAuthActivity) {
        this.view = view
    }

    fun unBind() {
        this.view = null
    }

    fun beginSpotifyAuth(contextActivity: Activity) {
        request = builder.build()
        AuthenticationClient.openLoginActivity(contextActivity, REQUEST_CODE, request)
    }

    fun validatingAuth(requestCode: Int, resultCode: Int, intent: Intent): Boolean {
        if (requestCode == REQUEST_CODE) {
            authenticationResponse = AuthenticationClient.getResponse(resultCode, intent)
            if (authenticationResponse.type == AuthenticationResponse.Type.TOKEN) {
                accessToken = authenticationResponse.accessToken
                return true
            }
        } else {
            view?.refreshAccessToken()
        }
        return false
    }
}
