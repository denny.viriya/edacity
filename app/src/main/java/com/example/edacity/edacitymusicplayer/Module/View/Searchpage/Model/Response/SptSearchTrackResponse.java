package com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response;

import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchTrack;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 01/01/18.
 */

public class SptSearchTrackResponse {
    @SerializedName("items")
    @Expose
    public List<SptSearchTrack> trackList = null;
}
