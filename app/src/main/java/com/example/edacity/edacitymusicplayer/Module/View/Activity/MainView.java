package com.example.edacity.edacitymusicplayer.Module.View.Activity;

import com.example.edacity.edacitymusicplayer.Module.Model.User;

/**
 * Created by denny on 10/12/17.
 * khusus digunakan presenter
 */

public interface MainView{
    void userDataArrived(User user);
}
