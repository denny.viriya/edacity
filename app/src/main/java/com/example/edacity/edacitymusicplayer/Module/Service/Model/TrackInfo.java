package com.example.edacity.edacitymusicplayer.Module.Service.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 10/01/18.
 */

public class TrackInfo {
    @SerializedName("track_id")
    @Expose
    private String id;

    @SerializedName("track_name")
    @Expose
    private String name;

    @SerializedName("track_uri")
    @Expose
    private String uri;

    @SerializedName("album_id")
    @Expose
    private String albumId;

    @SerializedName("album_name")
    @Expose
    private String albumName;

    @SerializedName("album_images")
    @Expose
    private List<AlbumImage> albumImages = null;

    @SerializedName("album_uri")
    @Expose
    private String albumUri;

    @SerializedName("artists")
    @Expose
    private List<Artist> artists = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getAlbumName() {
        return "Album: " + albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public List<AlbumImage> getAlbumImages() {
        return albumImages;
    }

    public void setAlbumImages(List<AlbumImage> albumImages) {
        this.albumImages = albumImages;
    }

    public String getAlbumUri() {
        return albumUri;
    }

    public void setAlbumUri(String albumUri) {
        this.albumUri = albumUri;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }
}
