package com.example.edacity.edacitymusicplayer.Module.Network.Response;

import com.example.edacity.edacitymusicplayer.Module.Model.Image;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 30/12/17.
 */

public class SptSearchItem {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("owner_id")
    @Expose
    public String ownerId;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("images")
    @Expose
    public List<SptImage> images;

    @SerializedName("uri")
    @Expose
    public String uri;
}
