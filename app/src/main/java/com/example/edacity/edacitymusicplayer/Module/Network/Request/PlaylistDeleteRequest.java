package com.example.edacity.edacitymusicplayer.Module.Network.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denny on 11/01/18.
 */

public class PlaylistDeleteRequest {
    @SerializedName("playlist_id")
    @Expose
    final String id;

    public PlaylistDeleteRequest(String id) {
        this.id = id;
    }
}
