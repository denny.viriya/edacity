package com.example.edacity.edacitymusicplayer.Module.View.Authentication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast

import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity
import com.example.edacity.edacitymusicplayer.R
import com.spotify.sdk.android.authentication.AuthenticationClient

class AuthActivity : AppCompatActivity(), AuthContracts.LoginFragmentCommunicator, AuthContracts.IAuthActivity {

    private val CLIENT_ID = "396415bf72d1417aaaf35e356ebdeec7"
    private val REDIRECT_URI = "edacity-android://callback"
    private var mAccessToken: String? = null

    companion object {
        lateinit var PACKAGE_NAME: String
        private lateinit var presenter: AuthPresenter
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        setFragment(LoginFragment())
        PACKAGE_NAME = applicationContext.packageName
        presenter = AuthPresenter()
        presenter.bind(this)
    }

    /**
     * Login with spotify
     */
    override fun onSpotifyLoginButtonClick() {
        presenter.beginSpotifyAuth(this)
    }

    /**
     * login with app
     */
    override fun onAppLoginButtonClick() {
        Toast.makeText(this, "Not implemented : Login", Toast.LENGTH_SHORT).show()
    }

    /**
     * register with app
     */
    override fun onAppRegisterButtonClick() {
        Toast.makeText(this, "Not implemented : Register", Toast.LENGTH_SHORT).show()
        setFragment(RegisterFragment())
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (presenter.validatingAuth(requestCode, resultCode, intent)) {
            mAccessToken = presenter.accessToken
            val mainActivityIntent = Intent(this, MainActivity::class.java)
            mainActivityIntent.putExtra("token", mAccessToken)
            mainActivityIntent.putExtra("clientid", CLIENT_ID)
            mainActivityIntent.putExtra("redirecturi", REDIRECT_URI)
            finish()
            startActivity(mainActivityIntent)
        }

    }

    /**
     * memulai fragment authentication
     * @param fragment framelayout untuk Loginfragment
     * @see LoginFragment
     */
    private fun setFragment(fragment: Fragment) {
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        ft.replace(R.id.auth_fragment, fragment)
        ft.commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }

    override fun refreshAccessToken() {
        AuthenticationClient.clearCookies(this)
        presenter.beginSpotifyAuth(this)
    }


}
