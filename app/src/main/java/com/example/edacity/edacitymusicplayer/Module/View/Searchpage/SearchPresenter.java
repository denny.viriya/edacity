package com.example.edacity.edacitymusicplayer.Module.View.Searchpage;

import android.os.Bundle;
import android.util.Log;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.Model.SptItemBasic;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivityViews;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.PlaylistFragment;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchAlbumResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchArtistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchPlaylistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchAlbum;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchArtist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchTrack;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by denny on 10/12/17.
 */

public class SearchPresenter {

    private static final String TAG = "SEARCH PRESENTER";
    private InteractorEdacity api;
    private SearchView view;
    private final String TRACK = "track";
    private final String PLAYLIST = "playlist";
    private final String ARTIST = "artist";
    private final String ALBUM = "album";
    private static String SEARCH_PLAYLIST = "search_playlist";
    private static String SEARCH_ALBUM = "search_album";
    private static String SEARCH_TRACK = "search_track";
    private static String SEARCH_ARTIST = "search_artist";
    private SptSearchArtistResponse artistResponse;
    private SptSearchAlbumResponse albumResponse;
    private SptSearchTrackResponse trackResponse;
    private SptSearchPlaylistResponse playlistResponse;
    private Bundle playlistDataBundle;
    private SptItemBasic compatPlaylist;
    private List<Playlistable> playlistableList;


    public SearchPresenter(InteractorEdacity api) {
        this.api = api;
    }

    public void bind(SearchView view){
        this.view = view;
    }

    public void unBind(){
        this.view = null;
    }

    public void beginSearch(String keyword, int limit, int offset){
        view.cleanSearchResult();
        executeSearchWithType(TRACK, keyword, limit, offset);
        executeSearchWithType(PLAYLIST, keyword, limit, offset);
        executeSearchWithType(ARTIST, keyword, limit, offset);
        executeSearchWithType(ALBUM, keyword, limit, offset);
    }

    private void executeSearchWithType(String type, String keyword, int limit, int offset){
        switch (type){
            case TRACK:{
                Call<SptSearchTrackResponse> sptSearchTrackResponseCall = api.getSearchTrack(TRACK, keyword, limit, offset);
                sptSearchTrackResponseCall.enqueue(new Callback<SptSearchTrackResponse>() {
                    @Override
                    public void onResponse(Call<SptSearchTrackResponse> call, Response<SptSearchTrackResponse> response) {
                        trackResponse = response.body();
                        view.appendResultTrack(trackResponse);
                    }

                    @Override
                    public void onFailure(Call<SptSearchTrackResponse> call, Throwable t) {

                    }
                });
                break;
            }
            case ALBUM:{
                Call<SptSearchAlbumResponse> sptSearchAlbumResponseCall = api.getSearchAlbum(ALBUM, keyword, limit, offset);
                sptSearchAlbumResponseCall.enqueue(new Callback<SptSearchAlbumResponse>() {
                    @Override
                    public void onResponse(Call<SptSearchAlbumResponse> call, Response<SptSearchAlbumResponse> response) {
                        albumResponse = response.body();
                        view.appendResultAlbum(albumResponse);

                    }

                    @Override
                    public void onFailure(Call<SptSearchAlbumResponse> call, Throwable t) {

                    }
                });
                break;
            }
            case PLAYLIST:{
                Call<SptSearchPlaylistResponse> sptSearchPlaylistResponseCall = api.getSearchPlaylist(PLAYLIST, keyword, limit, offset);
                sptSearchPlaylistResponseCall.enqueue(new Callback<SptSearchPlaylistResponse>() {
                    @Override
                    public void onResponse(Call<SptSearchPlaylistResponse> call, Response<SptSearchPlaylistResponse> response) {
                        playlistResponse = response.body();
                        view.appendResultPlaylist(playlistResponse);
                    }

                    @Override
                    public void onFailure(Call<SptSearchPlaylistResponse> call, Throwable t) {

                    }
                });
                break;
            }
            case ARTIST:{
                Call<SptSearchArtistResponse> sptSearchArtistResponseCall = api.getSearchArtist(ARTIST, keyword, limit, offset);
                sptSearchArtistResponseCall.enqueue(new Callback<SptSearchArtistResponse>() {
                    @Override
                    public void onResponse(Call<SptSearchArtistResponse> call, Response<SptSearchArtistResponse> response) {
                        artistResponse = response.body();
                        view.appendResultArtist(artistResponse);
                    }

                    @Override
                    public void onFailure(Call<SptSearchArtistResponse> call, Throwable t) {

                    }
                });
                break;
            }
        }
    }

    public void onItemSelected(SptSearchArtist item){
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(SEARCH_ARTIST, Parcels.wrap(item));
//        view.openPlaylistFragment(item, playlistDataBundle);
        playlistDataBundle = new Bundle();
        compatPlaylist = new SptItemBasic();

        compatPlaylist.setId(item.id)
                .setName(item.name)
                .setApiType(ApiType.ARTIST);

        if (item.images == null || item.images.isEmpty()){
        }else {
            compatPlaylist.setImageUrl(item.images.get(0).url);
        }

        playlistDataBundle.putParcelable(MainActivity.PLAYLIST_KEYWORD, Parcels.wrap(compatPlaylist));
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(playlistDataBundle);

        view.openPlaylistFragment(fragment);
    }

    public void onItemSelected(SptSearchAlbum item){
        playlistDataBundle = new Bundle();
        compatPlaylist = new SptItemBasic();

        compatPlaylist.setId(item.id)
                .setName(item.name)
                .setApiType(ApiType.ALBUM);

        if (item.images == null || item.images.isEmpty()){
        }else {
            compatPlaylist.setImageUrl(item.images.get(0).url);
        }

        playlistDataBundle.putParcelable(MainActivity.PLAYLIST_KEYWORD, Parcels.wrap(compatPlaylist));
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(playlistDataBundle);

        view.openPlaylistFragment(fragment);
    }

    /**
     * tidak usah ganti fragment, langsung saja mainkan track
     * @param item
     */
    public void onItemSelected(SptSearchTrack item){
        playlistableList = new ArrayList<>();
        item.apiType = ApiType.TRACK;
        playlistableList.add(item);
        view.playSingleTrack(playlistableList);
    }

    public void onItemSelected(SptSearchPlaylist item){
        playlistDataBundle = new Bundle();
        compatPlaylist = new SptItemBasic();

        compatPlaylist.setId(item.id)
                .setName(item.name)
                .setOwnerId(item.ownerId)
                .setApiType(ApiType.PLAYLIST);

        if (item.images == null || item.images.isEmpty()){
        }else {
            compatPlaylist.setImageUrl(item.images.get(0).url);
        }

        playlistDataBundle.putParcelable(MainActivity.PLAYLIST_KEYWORD, Parcels.wrap(compatPlaylist));
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(playlistDataBundle);

        view.openPlaylistFragment(fragment);
    }
}
