package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Album;

import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.SptPlaylistExternalUrls;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denny on 02/01/18.
 */

public class SptPlaylistAlbumArtist {
    @SerializedName("external_urls")
    @Expose
    public SptPlaylistExternalUrls externalUrls;
    @SerializedName("href")
    @Expose
    public String href;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("uri")
    @Expose
    public String uri;
}
