package com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model;

import com.example.edacity.edacitymusicplayer.Module.Model.SptItem;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptImage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by denny on 01/01/18.
 */
@Parcel
public class SptSearchPlaylist extends SptItem {
    @SerializedName("images")
    @Expose
    public List<SptImage> images = null;
    @SerializedName("owner_id")
    @Expose
    public String ownerId;
    @SerializedName("uri")
    @Expose
    public String uri;
}
