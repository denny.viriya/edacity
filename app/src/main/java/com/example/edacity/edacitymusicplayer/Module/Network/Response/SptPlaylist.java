package com.example.edacity.edacitymusicplayer.Module.Network.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by denny on 11/12/17.
 */
@Parcel
public class SptPlaylist {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("images")
    @Expose
    public List<SptImage> images = null;

    @SerializedName("owner_id")
    @Expose
    public String ownerId;

    @SerializedName("uri")
    @Expose
    public String uri;

}
