package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Adapter;

import android.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.AddPlaylistDialog;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.PlaylistFragment;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.PlaylistPagePresenter;
import com.example.edacity.edacitymusicplayer.R;

import java.util.List;

/**
 * Created by denny on 03/01/18.
 */

public class PlaylistGenericRVAdapter<T extends List<Playlistable>> extends RecyclerView.Adapter<PlaylistViewHolder> {
    private T playlistableList;
    private PlaylistPagePresenter presenter;
    private LayoutInflater inflater;
    private View view;
    private ApiType apiType;
    private AddPlaylistDialog addPlaylistDialog;
    private final String DIALOG_TAG = "open_dialog";
    private FragmentManager fm;

    public PlaylistGenericRVAdapter(T playlistableList, ApiType apiType) {
        this.playlistableList = playlistableList;
        this.apiType = apiType;
        presenter = PlaylistFragment.presenter;
    }

    public void setFragmentManager(FragmentManager fm){
        this.fm = fm;
    }

    @Override
    public PlaylistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.playlist_tracks_viewholder, parent, false);
        return new PlaylistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaylistViewHolder holder, int position) {
        holder.trackTitle.setText(playlistableList.get(position).getName());
        holder.trackDesc1.setText(playlistableList.get(position).getDescription1());
        holder.trackDesc2.setText(playlistableList.get(position).getDescription2());

        holder.trackOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onPlaylistItemSelected(playlistableList, holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return playlistableList.size();
    }
}

class PlaylistViewHolder extends RecyclerView.ViewHolder{
    TextView trackTitle, trackDesc1, trackDesc2;
    ImageButton trackOption;

    public PlaylistViewHolder(View itemView) {
        super(itemView);
        trackTitle = itemView.findViewById(R.id.track_title);
        trackDesc1 = itemView.findViewById(R.id.track_artist);
        trackDesc2 = itemView.findViewById(R.id.track_album);
        trackOption = itemView.findViewById(R.id.track_option);
    }
}