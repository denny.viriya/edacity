package com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response;

import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchPlaylist;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 01/01/18.
 */

public class SptSearchPlaylistResponse {
    @SerializedName("items")
    @Expose
    public List<SptSearchPlaylist> playlistList = null;
}
