package com.example.edacity.edacitymusicplayer.Module.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import java.util.List;

/**
 * Created by denny on 26/11/17.
 */
@Parcel
public class Result{

    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("artist")
    @Expose
    String artist;
    @SerializedName("play_count")
    @Expose
    String playCount;
    @SerializedName("listeners")
    @Expose
    String listeners;
    @SerializedName("image")
    @Expose
    List<Image> image = null;
    @SerializedName("id")
    @Expose
    String id;

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getPlayCount() {
        return playCount;
    }

    public String getListeners() {
        return listeners;
    }

    public List<Image> getImage() {
        return image;
    }

    public String getId() {
        return id;
    }
}
