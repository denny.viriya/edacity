package com.example.edacity.edacitymusicplayer.Module.Service;

/**
 * Created by denny on 09/01/18.
 */

public enum ActionEnum {
    PLAYSHUFFLE,
    PLAY,
    PAUSE,
    RESUME,
    STOP,
    NEXT,
    PREV;
}
