package com.example.edacity.edacitymusicplayer.Module.Model;

/**
 * Created by denny on 02/01/18.
 */

public enum ApiType {
    TRACK,
    PLAYLIST,
    ALBUM,
    ARTIST;
}
