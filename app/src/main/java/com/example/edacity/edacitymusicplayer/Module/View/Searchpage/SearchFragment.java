package com.example.edacity.edacitymusicplayer.Module.View.Searchpage;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.edacity.edacitymusicplayer.Module.Model.Tracks;
import com.example.edacity.edacitymusicplayer.Module.Network.InteractorEdacity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivity;
import com.example.edacity.edacitymusicplayer.Module.View.Activity.MainActivityViews;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.PlaylistFragment;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchAlbumResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchArtistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchPlaylistResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.Response.SptSearchTrackResponse;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchAlbum;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchArtist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchPlaylist;
import com.example.edacity.edacitymusicplayer.Module.View.Searchpage.Model.SptSearchTrack;
import com.example.edacity.edacitymusicplayer.R;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by denny on 29/11/17.
 */

public class SearchFragment extends Fragment implements SearchView{

    private static final String TAG = "SEARCH FRAGMENT";
    private SearchRecyclerViewAdapter mAdapterRV;
    private ImageButton searchButton;
    private EditText searchEditText;
    private View progressBar;
    private ViewGroup searchScreen;
    private TextView searchTitleAlbum, searchTitleTrack, searchTitlePlaylist, searchTitleArtist;
    private RecyclerView searchContentAlbum, searchContentTrack, searchContentPlaylist, searchContentArtist;
    private View resultLayoutAlbum, resultLayoutTrack, resultLayoutPlaylist, resultLayoutArtist;
    private MainActivityViews mainActivityViews;
    private static InteractorEdacity api;
    public static SearchPresenter presenter;
    private PlaylistFragment playlistFragment;
    private FragmentManager fragmentManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivityViews = (MainActivityViews) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (mainActivityViews==null){
            mainActivityViews = (MainActivityViews) activity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.search_fragment, container, false);
        mainActivityViews.toolbarHasBackButton(false);
        mainActivityViews.setTitle("Search");
        api = MainActivity.api;
        presenter = new SearchPresenter(api);
        presenter.bind(this);

        initView(view);

        hideView(progressBar);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyword = String.valueOf(searchEditText.getText());
                presenter.beginSearch(keyword,5,0);
            }
        });

        searchEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
                    // Perform action on key press
                    String keyword = String.valueOf(searchEditText.getText());
                    presenter.beginSearch(keyword,5,0);

                    return true;
                }
                return false;
            }
        });

        return view;
    }

    public void initView(View view) {
        searchButton = view.findViewById(R.id.search_button);
        searchEditText = view.findViewById(R.id.search_bar);
        progressBar = view.findViewById(R.id.search_progress_bar);
        searchScreen = view.findViewById(R.id.search_screen);

        resultLayoutAlbum = LayoutInflater
                .from(getActivity())
                .inflate(R.layout.search_result_view, searchScreen, false);
        resultLayoutTrack = LayoutInflater
                .from(getActivity())
                .inflate(R.layout.search_result_view, searchScreen, false);
        resultLayoutArtist = LayoutInflater
                .from(getActivity())
                .inflate(R.layout.search_result_view, searchScreen, false);
        resultLayoutPlaylist = LayoutInflater
                .from(getActivity())
                .inflate(R.layout.search_result_view, searchScreen, false);

        searchTitleAlbum = resultLayoutAlbum.findViewById(R.id.search_result_title);
        searchTitleTrack = resultLayoutTrack.findViewById(R.id.search_result_title);
        searchTitleArtist = resultLayoutArtist.findViewById(R.id.search_result_title);
        searchTitlePlaylist = resultLayoutPlaylist.findViewById(R.id.search_result_title);
        searchContentAlbum = resultLayoutAlbum.findViewById(R.id.search_result_content);
        searchContentArtist = resultLayoutArtist.findViewById(R.id.search_result_content);
        searchContentTrack = resultLayoutTrack.findViewById(R.id.search_result_content);
        searchContentPlaylist = resultLayoutPlaylist.findViewById(R.id.search_result_content);
    }

    public void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    public void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    public void bringToFront(View view) {
        view.bringToFront();
    }

    @Override
    public void hideProgressBar() {
        hideView(progressBar);
    }

    @Override
    public void showProgressbar() {
        showView(progressBar);
        bringToFront(progressBar);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unBind();
    }

    @Override
    public void cleanSearchResult() {
        searchScreen.removeAllViews();
    }

    @Override
    public void appendResultTrack(SptSearchTrackResponse response) {
        mAdapterRV = new SearchRecyclerViewAdapter(presenter, getActivity(), response);
        if (!response.trackList.isEmpty()){
            searchTitleTrack.setText("Track");
            searchContentTrack.setNestedScrollingEnabled(false);
            searchContentTrack.setLayoutManager(
                    new LinearLayoutManager(getActivity(),
                            LinearLayoutManager.VERTICAL,
                            false)
            );
            searchContentTrack.setAdapter(mAdapterRV);
            searchScreen.addView(resultLayoutTrack);
        }else {
            Log.d(TAG, "appendResultTrack: EMPTY LIST => " + response.trackList.size());
        }

    }

    @Override
    public void appendResultArtist(SptSearchArtistResponse response) {
        mAdapterRV = new SearchRecyclerViewAdapter(presenter, getActivity(), response);
        if (!response.artistList.isEmpty()){
            searchTitleArtist.setText("Artist");
            searchContentArtist.setNestedScrollingEnabled(false);
            searchContentArtist.setLayoutManager(
                    new LinearLayoutManager(getActivity(),
                            LinearLayoutManager.VERTICAL,
                            false)
            );
            searchContentArtist.setAdapter(mAdapterRV);
            searchScreen.addView(resultLayoutArtist);
        }else {
            Log.d(TAG, "appendResultArtist: EMPTY LIST => " + response.artistList.size());
        }
    }

    @Override
    public void appendResultAlbum(SptSearchAlbumResponse response) {
        mAdapterRV = new SearchRecyclerViewAdapter(presenter, getActivity(), response);
        if (!response.albumList.isEmpty()){
            searchTitleAlbum.setText("Album");
            searchContentAlbum.setNestedScrollingEnabled(false);
            searchContentAlbum.setLayoutManager(
                    new LinearLayoutManager(getActivity(),
                            LinearLayoutManager.VERTICAL,
                            false)
            );
            searchContentAlbum.setAdapter(mAdapterRV);
            searchScreen.addView(resultLayoutAlbum);

        }else {
            Log.d(TAG, "appendResultAlbum: EMPTY LIST => " + response.albumList.size());

        }
    }

    @Override
    public void appendResultPlaylist(SptSearchPlaylistResponse response) {
        mAdapterRV = new SearchRecyclerViewAdapter(presenter, getActivity(), response);
        if (!response.playlistList.isEmpty()){
            searchTitlePlaylist.setText("Playlist");
            searchContentPlaylist.setNestedScrollingEnabled(false);
            searchContentPlaylist.setLayoutManager(
                    new LinearLayoutManager(getActivity(),
                            LinearLayoutManager.VERTICAL,
                            false)
            );
            searchContentPlaylist.setAdapter(mAdapterRV);
            searchScreen.addView(resultLayoutPlaylist);

        }else {
            Log.d(TAG, "appendResultPlaylist: EMPTY LIST => " + response.playlistList.size());

        }
    }

    @Override
    public void openPlaylistFragment(Fragment fragment) {
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment,fragment)
                .addToBackStack("home1")
                .commit();
    }

    @Override
    public void playSingleTrack(List<Playlistable> playlistable) {
        mainActivityViews.shufflePlay(playlistable);
    }
}
