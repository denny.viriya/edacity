package com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Album;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.SptPlaylistExternalUrls;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 02/01/18.
 */

public class SptPlaylistAlbumItem implements Playlistable {
    @SerializedName("artists")
    @Expose
    public List<SptPlaylistAlbumArtist> artists = null;
    @SerializedName("available_markets")
    @Expose
    public List<String> availableMarkets = null;
    @SerializedName("disc_number")
    @Expose
    public Integer discNumber;
    @SerializedName("duration_ms")
    @Expose
    public Integer durationMs;
    @SerializedName("explicit")
    @Expose
    public Boolean explicit;
    @SerializedName("external_urls")
    @Expose
    public SptPlaylistExternalUrls externalUrls;
    @SerializedName("href")
    @Expose
    public String href;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("preview_url")
    @Expose
    public Object previewUrl;
    @SerializedName("track_number")
    @Expose
    public Integer trackNumber;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("uri")
    @Expose
    public String uri;

    public ApiType apiType;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription1() {
        if (artists!=null){
            if (artists.size()>0){
                String s = "";
                for (SptPlaylistAlbumArtist item: artists) {
                    s += item.name;
                    s += " ";
                }
                return s;
            }else {
                return "-";
            }
        }else{
            return "-";
        }
    }

    @Override
    public String getDescription2() {
        Integer durationS = durationMs/1000;
        String s = "duration: " + durationS.toString() + "s";
        return s;
    }

    @Override
    public ApiType getApiType() {
        return apiType;
    }
}
