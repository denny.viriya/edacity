package com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 11/01/18.
 */

public class MyPlaylist {
    @SerializedName("playlist_name")
    @Expose
    public String playlistName;
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("tracks")
    @Expose
    public List<Object> tracks = null;
}
