package com.example.edacity.edacitymusicplayer.Module.View.Homepage;

import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.edacity.edacitymusicplayer.Module.Model.Tracks;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptCategory;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptFeaturedPlaylist;
import com.example.edacity.edacitymusicplayer.Module.Network.Response.SptPlaylist;

import java.util.List;

/**
 * Created by denny on 10/12/17.
 */

public interface HomeView {
    void updateTitle(List<SptCategory> sptCategoryList);
    void updateCardview(List<SptPlaylist> sptPlaylistList, int recyclerView);
    void hideView(View view);
    void showView(View view);
    void openPlaylistFragment(Fragment fragment);
}
