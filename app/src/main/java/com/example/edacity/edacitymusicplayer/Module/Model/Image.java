package com.example.edacity.edacitymusicplayer.Module.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by denny on 26/11/17.
 */
@Parcel
public class Image{
    @SerializedName("#text")
    @Expose
    String text;
    @SerializedName("size")
    @Expose
    String size;

    public String getText() {
        return text;
    }

    public String getSize() {
        return size;
    }
}
