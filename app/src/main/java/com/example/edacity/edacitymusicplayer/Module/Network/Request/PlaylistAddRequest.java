package com.example.edacity.edacitymusicplayer.Module.Network.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by denny on 29/12/17.
 */

public class PlaylistAddRequest {
    @SerializedName("playlist_name")
    @Expose
    final String name;

    public PlaylistAddRequest(String name) {
        this.name = name;
    }
}
