package com.example.edacity.edacitymusicplayer.Module.Network.Response;

import com.example.edacity.edacitymusicplayer.Module.Model.ApiType;
import com.example.edacity.edacitymusicplayer.Module.View.PlaylistPage.Model.Playlistable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 23/12/17.
 */

public class SptPlaylistTrack implements Playlistable{
    @SerializedName("track_id")
    @Expose
    public String trackId;

    @SerializedName("track_name")
    @Expose
    public String trackName;

    @SerializedName("popularity")
    @Expose
    public Integer popularity;

    @SerializedName("duration")
    @Expose
    public Integer duration;

    @SerializedName("album")
    @Expose
    public String album;

    @SerializedName("artists")
    @Expose
    public List<SptPlaylistArtist> artists = null;

    public ApiType apiType;

    @Override
    public String getId() {
        return this.trackId;
    }

    @Override
    public String getName() {
        return this.trackName;
    }

    @Override
    public String getDescription1() {
        if (artists!=null){
            if (artists.size()>0){
                StringBuilder s = new StringBuilder();
                for (SptPlaylistArtist item: artists) {
                    s.append(item.artistName);
                    s.append(" ");
                }
                return s.toString();
            }else {
                return "-";
            }
        }else{
            return "-";
        }
    }

    @Override
    public String getDescription2() {
        return album;
    }

    @Override
    public ApiType getApiType() {
        return apiType;
    }
}
