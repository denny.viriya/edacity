package com.example.edacity.edacitymusicplayer.Module.View.Playpage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by denny on 10/01/18.
 */

public class PlayCollectionPagerAdapter extends FragmentStatePagerAdapter {
    private String playlistArray[];
    private int shufflePlayIndex;

    public PlayCollectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setData(String[] playlistArray, int position){
        this.playlistArray = playlistArray;
        this.shufflePlayIndex = position;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new TrackSubstanceFragment();
        Bundle args = new Bundle();

        args.putString("id", playlistArray[position]);
        args.putInt(TrackSubstanceFragment.ARG_OBJECT, position+1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return playlistArray.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Track : " + (position + 1);
    }
}
