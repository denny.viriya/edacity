package com.example.edacity.edacitymusicplayer.Module.View.MyLibrarypage.Model;

import com.example.edacity.edacitymusicplayer.Module.Network.Response.ApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denny on 11/01/18.
 */

public class UserPlaylistData{
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("playlists")
    @Expose
    public List<MyPlaylist> playlists = null;
}
